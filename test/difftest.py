#!/usr/bin/python

import sys
import subprocess
import os

# Workaround to have argparse added
if sys.version_info < (2, 7):
    sys.path.append("..")

import argparse


# this is a simple test which runs an executable and compares it's output to expected output using diff tool

class DiffTestException(Exception):
    def __init__(self,msg,stdout_str,stderr_str):
        self.msg = msg
        self.stdout_str = stdout_str
        self.stderr_str = stderr_str


class TestCfg:
    def __init__(self, name):
        self.name = name
        self.command = ""
        self.output = ""
        self.expected_output = ""
        self.difftool = "diff"

    def setDiffTool(self, difftool):
        self.difftool = difftool

    def __str__(self):
        return "{\nname=%s,\ncommand=%s,\noutput=%s,\nexpected=%s,\ndifftool=%s\n}" % \
               (self.name, self.command, self.output, self.expected_output, self.difftool)

class TestRun:
    def __init__(self, env):
        self.tests = []

    def addTest(self, test):
        self.tests.append(test)


def run_diff_test(command, output, expected_output, difftool, env):

    onlyRunCmd = True if output == "$NULL" or expected_output == "$NULL" else False

    #cleanup expected output
    if os.path.exists(output):
        os.remove(output)    
        

    cmd_args = command.split()

    #print cmd_args

    sysenv = dict(os.environ)
    sysenv["LD_LIBRARY_PATH"] = env["$BIN_DIR"]

    proc = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=sysenv)
    
    stdout_str, stderr_str = proc.communicate()
    if proc.returncode:
        raise DiffTestException("Test command crashed",stdout_str, stderr_str)

    
    if onlyRunCmd:
        return

    diff_args = [ difftool, output, expected_output ]
    diff_proc = subprocess.Popen(diff_args, stdout = subprocess.PIPE, stderr = subprocess.PIPE)   

    stdout_str, stderr_str = diff_proc.communicate()

    if diff_proc.returncode:
        raise DiffTestException("TEST FAILED", stdout_str, stderr_str)


def parseConfig(path, env):
    cfgfile = open(path)

    modifiers = {}
    testCfgMap = {}
    tests= []

    print "Running suite %s ..." % path

    env["$TEST_DIR"] = os.path.dirname(os.path.abspath( path) )

    for line in cfgfile:
        if line.startswith("#") or len(line.strip()) == 0:
            continue

        items = line.strip().split('=')
        
        if len(items) != 2:
            print "Wrong configuration line: %s" % line
            continue

        if items[0].startswith("$"):
            name = items[0]
            mod_value = items[1]
            for item in env:
                mod_value = mod_value.replace( item, env[item] )
            modifiers[name] = mod_value
        else:
            testCfgMap[items[0]] = items[1]

    for testName in testCfgMap:
        cfgLine = testCfgMap[testName]
        for m in modifiers:
            cfgLine = cfgLine.replace(m, modifiers[m])
        for item in env:
            cfgLine = cfgLine.replace(item, env[item])

        tItems = cfgLine.split("||")
        if len(tItems) < 3:
            print "Wrong test configuration: %s" % cfgLine
            continue

        test = TestCfg(testName)
        test.command = tItems[0]
        test.output = tItems[1]
        test.expected_output = tItems[2]
        if len(tItems) > 3:
            test.setDiffTool(tItems[3])

        tests.append(test)

    return tests



def runTestSuite(cfg, env, filteredName, verbose):

    finishedTests = []
    tests = parseConfig(cfg, env)

    for test in tests:

        fullTestName = cfg.replace("tests.cfg","") +  test.name

        if filteredName:
            if filteredName != test.name:
                continue
        if verbose:
            print "\nRunning %s\n" % test

        try:

            run_diff_test( test.command , test.output, test.expected_output, test.difftool, env )
            finishedTests.append( (fullTestName, True) )

        except DiffTestException as e:

            print e.msg, "\n"
            print "Std out:"
            print e.stdout_str
            print "Std err:"
            print e.stderr_str
            finishedTests.append(  (fullTestName, False) )

    return finishedTests


def loadEnvironment():

    env = {}

    rootDir = os.path.abspath( os.path.curdir )
    env["$TEST_ROOT_DIR"] = rootDir
    env["$COMMON_DATA_DIR"] = os.path.join(rootDir, "common_data")
    env["$BIN_DIR"] = os.path.abspath(rootDir + "/../build/Debug")
    env["$APP_DIR"] = os.path.abspath(rootDir + "/../")

    return env

if __name__ == "__main__":

    descriptionText = "DiffTest: a very simple data-driven test framework."
                
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
                                        
    parser.add_argument("-c", "--config", dest="config", default ="", help="Single suite conifguration file. Mutually exclusive with --test-set parameter.")
    parser.add_argument("-n", "--name", dest="testName", default="", help="In case running only single test, provide its name as parameter")
    parser.add_argument("-s", "--test-set", dest="testSet", default ="", help="File with containging set of test suites to run.")
    parser.add_argument("-v", "--verbose", action="store_true", dest="verbose", default=False, help="Verbose output for tests.")

    #parser.add_argument("-e", "--env", dest="env", default="", help="Environment variables for test. If none is give some default values are used.")

    args = parser.parse_args() 
    filteredName = args.testName
    verbose = args.verbose

    env = loadEnvironment()
    if verbose:
        print "Environment: %s\n" % env

    configs = []

    if len(args.config) > 0 and len(args.testSet) > 0:
        print "Parameter -c and -s are mutually exclusive. Please provide either test set or a path to single suite."
        sys.exit(-1)

    if args.config:
        if os.path.isdir(args.config):
            args.config = os.path.join(args.config,"tests.cfg")
            if verbose:
                print "Only folder is provided, assuming tests.cfg... "
        configs.append(args.config)
    else:
        filelist = []
        if args.testSet:
            for line in open(args.testSet):
                line = line.strip()
                if line.startswith("#") or len(line) == 0:
                    continue
                filelist.append(line)
        else:
            print "Test set is not provided, running all suites found in current folder."
            filelist = os.listdir(".")

        for infile in filelist:
            if os.path.isdir(infile):
                cfg = os.path.join(infile,"tests.cfg")
                if os.path.exists(cfg):
                    configs.append(cfg)
                    if verbose:
                        print "Found test config:", cfg

    finishedTests = []
    for cfg in configs:
        finishedTests += runTestSuite(cfg, env, filteredName, verbose)

    if not finishedTests:
        print "\nNO TESTS FOUND\n"
        sys.exit(0)

    print "Total number of run tests:", len(finishedTests)

    failedTests = []
    for test in finishedTests:
        if not test[1]:
            failedTests.append( test[0] )

    print
    if not failedTests:
        print "ALL TESTS PASSED"
    else:
        print "FAILED TESTS:"
        print
        for t in failedTests:
            print t

    print






