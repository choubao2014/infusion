#!/bin/bash


cut -f 2- $1 > /tmp/fusions_report1.tmp
cut -f 2- $2 > /tmp/fusions_report2.tmp

diff /tmp/fusions_report1.tmp /tmp/fusions_report2.tmp

rm /tmp/fusions_report1.tmp
rm /tmp/fusions_report2.tmp
