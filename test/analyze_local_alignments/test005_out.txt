@HD	VN:1.0	SO:unsorted
@SQ	SN:10	LN:135534747
@SQ	SN:11	LN:135006516
@SQ	SN:12	LN:133851895
@SQ	SN:13	LN:115169878
@SQ	SN:14	LN:107349540
@SQ	SN:15	LN:102531392
@SQ	SN:16	LN:90354753
@SQ	SN:17	LN:81195210
@SQ	SN:18	LN:78077248
@SQ	SN:19	LN:59128983
@SQ	SN:1	LN:249250621
@SQ	SN:20	LN:63025520
@SQ	SN:21	LN:48129895
@SQ	SN:22	LN:51304566
@SQ	SN:2	LN:243199373
@SQ	SN:3	LN:198022430
@SQ	SN:4	LN:191154276
@SQ	SN:5	LN:180915260
@SQ	SN:6	LN:171115067
@SQ	SN:7	LN:159138663
@SQ	SN:8	LN:146364022
@SQ	SN:9	LN:141213431
@SQ	SN:MT	LN:16569
@SQ	SN:X	LN:155270560
@SQ	SN:Y	LN:59373566
@PG	ID:bowtie2	PN:bowtie2	VN:2.0.2
R_33171:1	1	16	56667247	9	27S48M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:85	XS:i:68	XN:i:0	XM:i:2	XO:i:0	XG:i:0	NM:i:2	MD:Z:2C11G33	YT:Z:UU	AS:i:85	XS:i:68	XN:i:0	XM:i:2	XO:i:0	XG:i:0	NM:i:2	MD:Z:2C11G33	YT:Z:UU	YM:i:1
R_33171:1	273	15	62242555	9	41S34M	*	0	0	CACTCTTTGCACGTGCAGGAGCCGGTGCAGGCGAAGGAGACACCTTCAGTCTCTGTTTGGTACTGTCATGAATAG	181166788356889826893782431399631578517864278199618228323647256272457757777	AS:i:68	XS:i:68	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:34	YT:Z:UU	AS:i:68	XS:i:68	XN:i:0	XM:i:0	XO:i:0	XG:i:0	NM:i:0	MD:Z:34	YT:Z:UU	YM:i:1
R_33171:1	257	16	56686478	9	27S48M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:63	XS:i:68	XN:i:0	XM:i:6	XO:i:0	XG:i:0	NM:i:6	MD:Z:2C5G5G0T6G12A12	YT:Z:UU	AS:i:63	XS:i:68	XN:i:0	XM:i:6	XO:i:0	XG:i:0	NM:i:6	MD:Z:2C5G5G0T6G12A12	YT:Z:UU	YM:i:1
R_33171:1	273	16	56701247	9	45M30S	*	0	0	CACTCTTTGCACGTGCAGGAGCCGGTGCAGGCGAAGGAGACACCTTCAGTCTCTGTTTGGTACTGTCATGAATAG	181166788356889826893782431399631578517864278199618228323647256272457757777	AS:i:62	XS:i:68	XN:i:0	XM:i:5	XO:i:0	XG:i:0	NM:i:5	MD:Z:12T9T2C5T1C11	YT:Z:UU	AS:i:62	XS:i:68	XN:i:0	XM:i:5	XO:i:0	XG:i:0	NM:i:5	MD:Z:12T9T2C5T1C11	YT:Z:UU	YM:i:1
R_33171:1	257	16	56704413	9	27S48M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:62	XS:i:68	XN:i:0	XM:i:6	XO:i:0	XG:i:0	NM:i:6	MD:Z:2C5G5G7G12A7A4	YT:Z:UU	AS:i:62	XS:i:68	XN:i:0	XM:i:6	XO:i:0	XG:i:0	NM:i:6	MD:Z:2C5G5G7G12A7A4	YT:Z:UU	YM:i:1
R_33171:1	257	16	56692585	9	30S45M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:58	XS:i:68	XN:i:0	XM:i:6	XO:i:0	XG:i:0	NM:i:6	MD:Z:11G1A5G1T2T7A12	YT:Z:UU	AS:i:58	XS:i:68	XN:i:0	XM:i:6	XO:i:0	XG:i:0	NM:i:6	MD:Z:11G1A5G1T2T7A12	YT:Z:UU	YM:i:1
R_33171:1	257	4	69242069	9	32S43M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:58	XS:i:68	XN:i:0	XM:i:5	XO:i:0	XG:i:0	NM:i:5	MD:Z:3A5G1A5G12A12	YT:Z:UU	AS:i:58	XS:i:68	XN:i:0	XM:i:5	XO:i:0	XG:i:0	NM:i:5	MD:Z:3A5G1A5G12A12	YT:Z:UU	YM:i:1
R_33171:1	257	16	56670346	9	27S48M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:57	XS:i:68	XN:i:0	XM:i:7	XO:i:0	XG:i:0	NM:i:7	MD:Z:2C5G5G1A1G3G12A12	YT:Z:UU	AS:i:57	XS:i:68	XN:i:0	XM:i:7	XO:i:0	XG:i:0	NM:i:7	MD:Z:2C5G5G1A1G3G12A12	YT:Z:UU	YM:i:1
R_33171:1	257	16	56660373	9	27S48M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:57	XS:i:68	XN:i:0	XM:i:7	XO:i:0	XG:i:0	NM:i:7	MD:Z:2T5G5G1A1G3G12A12	YT:Z:UU	AS:i:57	XS:i:68	XN:i:0	XM:i:7	XO:i:0	XG:i:0	NM:i:7	MD:Z:2T5G5G1A1G3G12A12	YT:Z:UU	YM:i:1
R_33171:1	273	1	237167602	9	39M36S	*	0	0	CACTCTTTGCACGTGCAGGAGCCGGTGCAGGCGAAGGAGACACCTTCAGTCTCTGTTTGGTACTGTCATGAATAG	181166788356889826893782431399631578517864278199618228323647256272457757777	AS:i:55	XS:i:68	XN:i:0	XM:i:4	XO:i:0	XG:i:0	NM:i:4	MD:Z:4T7T12C7T5	YT:Z:UU	AS:i:55	XS:i:68	XN:i:0	XM:i:4	XO:i:0	XG:i:0	NM:i:4	MD:Z:4T7T12C7T5	YT:Z:UU	YM:i:1
R_33171:1	257	20	33805839	9	31S44M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:54	XS:i:68	XN:i:0	XM:i:6	XO:i:0	XG:i:0	NM:i:6	MD:Z:4G5G1A1G3G12A12	YT:Z:UU	AS:i:54	XS:i:68	XN:i:0	XM:i:6	XO:i:0	XG:i:0	NM:i:6	MD:Z:4G5G1A1G3G12A12	YT:Z:UU	YM:i:1
R_33171:1	273	1	33417010	9	44M31S	*	0	0	CACTCTTTGCACGTGCAGGAGCCGGTGCAGGCGAAGGAGACACCTTCAGTCTCTGTTTGGTACTGTCATGAATAG	181166788356889826893782431399631578517864278199618228323647256272457757777	AS:i:48	XS:i:68	XN:i:0	XM:i:7	XO:i:0	XG:i:0	NM:i:7	MD:Z:12T12C3C0A0T1C5C4	YT:Z:UU	AS:i:48	XS:i:68	XN:i:0	XM:i:7	XO:i:0	XG:i:0	NM:i:7	MD:Z:12T12C3C0A0T1C5C4	YT:Z:UU	YM:i:1
R_33171:1	257	16	56652086	9	27S48M	*	0	0	CTATTCATGACAGTACCAAACAGAGACTGAAGGTGTCTCCTTCGCCTGCACCGGCTCCTGCACGTGCAAAGAGTG	777757754272652746323822816991872468715875136993134287398628988653887661181	AS:i:47	XS:i:68	XN:i:0	XM:i:9	XO:i:0	XG:i:0	NM:i:9	MD:Z:2C3G1G5G1T4T0G2A9A12	YT:Z:UU	AS:i:47	XS:i:68	XN:i:0	XM:i:9	XO:i:0	XG:i:0	NM:i:9	MD:Z:2C3G1G5G1T4T0G2A9A12	YT:Z:UU	YM:i:1
