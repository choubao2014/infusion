import os
import datetime

__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import argparse
import sys
import run_comparison

sys.path.append("..")

import matplotlib.pyplot as plt
import numpy as np
import pandas


TOOL_NAME = "InFusion"


def plotResults(results, resultsName, expectedResults, expectedResultsName, dirpath ):

    #x = []
    #y1 = []
    #y2 = []
    
    data = []
    labels = []

    if len(expectedResults) > 0:
        data.append(expectedResults)
        labels.append(expectedResultsName)

    data.append(results)
    labels.append(resultsName)

    #for item in data:
    #    x.append( item[0] )
    #    y1.append( item[1] )
    #    y2.append( item[2] )
    
    fig, ax = plt.subplots()
    #ax.plot(x, y1, label="Validated", color = 'g')
    #ax.plot(x, y2, label="Total", color = 'b' )
    #ax.hlines(num_expected, 0, max(x), label="Total validated", color = 'r')

    ax.boxplot(data  ) 


    #frame  = legend.get_frame()
    #frame.set_facecolor('0.90')
  
    #for label in legend.get_texts():
    #    label.set_fontsize('large')

    #for label in legend.get_lines():
    #    label.set_linewidth(1.5)  # the legend line width
    
    space_str = " " * 50
    plt.xlabel(expectedResultsName + space_str + resultsName + " subsamples")
    plt.ylabel("Number of fusions detected")
    plt.title("Detection of fusions from subsamples")

    plt.savefig(dirpath + "/subsampling_result.png")

#def getSampleStats(fusions, expectedFusions):
    
#    tp = 0
#    for f in fusions:
#        foundFusion  =  run_comparison.addFusionToPool(f, expectedFusions, TOOL_NAME, create=False)
#        if foundFusion:
#            tp += 1
    
#    return tp, len(fusions)



if __name__ == "__main__":

    descriptionText = "The script compares results of InFusion when multiple data subsamples are analyzed and results are available. TODO: If there are some expected number of fusions then it can be compared."

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("sample_list", help="File which contains names of dirs corresponding for each InFusion subsample output")
    parser.add_argument("-c", dest="pathToCompare", default="", help="Path to list of fusions to compare with")
    parser.add_argument("-sn", dest="resultsName", default="R", help="Sample name")
    parser.add_argument("-cn", dest="compareName", default="CR", help="Name of fusions to compare with")

    args = parser.parse_args()

    results = []

    dirpath = os.path.dirname(args.sample_list)

    expectedResults = []
    
    for line in open(args.sample_list):
        if not line.strip() or line.startswith("#"):
            continue

        items = line.strip().split()

        #expectedFusions = run_comparison.loadExpectedFusions(args.pathToExpected)
        #numExpected = len(expectedFusions)

        dataset_name = items[0]
        tool = run_comparison.InFusion()
        fusionsPath = os.path.join(dirpath,dataset_name, "fusions.txt")

        if not os.path.exists(fusionsPath):
            print "File %s is not found. Skip...." % (fusionsPath)
            continue

        print "Parsing",fusionsPath
        
        tool.expectedFusionsOutput = fusionsPath
        fusions = tool.getFusionsOutput()

        #tp, total = getSampleStats(fusions, expectedFusions)
        #print len(fusions)

        results.append(len(fusions))

        #print datset_name, tp, total
        #results.append( (sampleSize,tp,total) ) 
    

    if len(args.pathToCompare) > 0:
        print "Loading list of fusions to compare with from", args.pathToCompare
        tool = run_comparison.InFusion()
        tool.expectedFusionsOutput = args.pathToCompare
        fusions = tool.getFusionsOutput( )
        expectedResults.append( len(fusions) ) 
        print "Number of fusions to compare with: ", len(fusions)
        
    print
    print "Results: ", results
    print "Mean = ", np.mean(results), ", std = ", np.std(results)
    # TODO: add names to command
    plotResults(results, args.resultsName, expectedResults, args.compareName, dirpath)

