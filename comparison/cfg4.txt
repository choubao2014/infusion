<fusionMap>
run_cmd=mono /home/kokonech/tools/FusionMap/bin/FusionMap.exe --semap /home/kokonech/tools/FusionMap hg19.17_20 RefGene /home/kokonech/playgrnd/infusion/simulation/case1.fusionmap.cfg
fusions_output=/home/kokonech/playgrnd/infusion/simulation/case1/fusion_map_output/fusions.txt
</fusionMap>

<infusion>
run_cmd=/home/kokonech/playgrnd/infusion/infusion /home/kokonech/playgrnd/infusion/simulation/case1.infusion.cfg
fusions_output=/home/kokonech/playgrnd/infusion/simulation/case1/fusionmap/fusions.txt
</infusion>

