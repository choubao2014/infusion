__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import sys
import os

dirpath = os.path.dirname(__file__)
sys.path.append( os.path.join(dirpath, "..") )

from fusion import *
import argparse


def parsePredictedList(inputFile):


    fusions = []

    for line in open(inputFile):
        if len(line.strip()) == 0 or line.startswith("#"):
            continue

        items = line.split()

        f = Fusion(int(items[0]))
        f.genes1 = items[9]
        f.genes2 = items[10]
        f.numSpans = int(items[7])
        f.numPaired = int(items[8])
        if f.numSpans == 0:
            continue
        fusions.append(f)

    return fusions

def parseDesignedList(inputFile):

    fusions = []

    fId = 1

    for line in open(inputFile):
        if len(line.strip()) == 0 or line.startswith("#"):
            continue

        items = line.split()

        f = Fusion(fId)
        f.genes1 = items[0].split(";")
        f.genes2 = items[1].split(";")

        fusions.append(f)
        fId += 1

        if len(items) >= 6:
            f.i1.ref = items[2]
            f.i2.ref = items[3]
            f.i1.bpos = int(items[4])
            f.i2.bpos = int(items[5])

        if len(items) >= 7:
            f.name = items[6]




    return fusions



def compareDesignedToPredicted(designedFusions, predictedFusions):

    numDetected = 0

    for df in designedFusions:
        varCount = 0
        score = 0
        for pf in predictedFusions:
            if df.genes1 in pf.genes1 and df.genes2 in pf.genes2:
                varCount += 1
                score += pf.numSpans + pf.numPaired
                pf.used = True
        df.varCount = varCount
        df.predictedScore = score
        if varCount > 0:
            numDetected += 1


    print "Num detected fusions: %d" % (numDetected)
    print "Num non-detected fusions: %d" % (len(designedFusions) - numDetected )
    print "Num false positive: %d" % (len(predictedFusions) - numDetected)

    print "\nDetected list:"
    for f in designedFusions:
        print "%s-%s %d (score: %d)" % (f.genes1, f.genes2, f.varCount, f.predictedScore)

    print "\nFalse positives list:"
    for f in predictedFusions:
        if not hasattr(f, 'used'):
            print "%s-%s" % (f.genes1, f.genes2 )



if __name__ == "__main__":

    descriptionText = "Comparison of tools for fusion detection"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("designed", help="Designed fusions list")
    parser.add_argument("predicted", help="Predicted by InFusion list")

    args = parser.parse_args()

    designedFusions = parseDesignedList(args.designed)
    predictedFusions = parsePredictedList(args.predicted)

    compareDesignedToPredicted(designedFusions, predictedFusions)
