Version Release History
=======================


Version 0.7.2 - December 7, 2015
--------------------------------
 
  * Added an option to support for SPLIT reads discovery from transcriptome
  * Added splice motif column to detailed fusions report (suggested by reviewer)
  * Added option to control min number of novel junction SPLIT reads
  * Updated background coverage filter, added an option to control
  * Fixed issue with removing generated global BAM file after the analysis
  * Fixed installation script (reported by reviewer)
  * Fixed overlapping SPLIT reads issue 
  * Added script to compare multiple simulation result datasets 
  * simulation pipeline: implemented external configuration basis
  * simulation pipeline: additional options control including read size, insert size, strand-specificity
  * run_comparison: added support for  FusionCatcher
  * run_comparison: support breakpoing precision discovery    
 

Version 0.7.1 - April 1, 2015
-----------------------------
  * cluster_canidates: fixed issue with unsupported chromosomes in GTF file


Version 0.7 - Feburary 18, 2015
-------------------------------

  * Added fusion exon distribution plot
  * Added analysis of fusion coverage context
  * Added Venn diagram code generation when comapring samples
  * Added option to output fusion read alignment including all data
  * output_fusion_alignments: fixed a bug with the header
  * filter_fusion_candidates: allowed to skip fusions from intronic regions
  * filter_fusion_candidates: changed default threshold for novel junctions
  * output_fusion_alignments: fixed several bugs and added to main pipeline
  * Fixed problem with mismatches
  * Update main scripts: added support to control fusions with introns
  * Simulation: added generation of expression profile based on counts file
  * Updatated sample.cfg example
    

Version 0.6.2 - Apr 16, 2014
----------------------------

  * Optimize default parameters
  * Fix flag for non-multimapped reads
  * Fix format detection
  * setup_reference_dataset: fix bug with output dir


Version 0.6.1 - Mar 24, 2014
----------------------------

  * Fix critical bug with mate-pair identification
  * Add match score and mismatch penalty
  * Intergrated compute_read_stats tool
  * Add RPKM estimation


Version 0.6 - Mar 9, 2014
-------------------------

  * Added detection of readthroughs
  * Improved filtering of fusions including intergenic and intronic regions
  * Fix bug with non-standard chromosomes
  * Fix bug with default library protocol
  * Add new method to output fusion alignments 
  * Simulation analysis: breakpoint precision 


Version 0.5.4 - Feb 12, 2014
----------------------------
 
  * Added new filtering parameters
  * Updated comparison and simulation pipelines
  * Fixed final fusions sorting
  * Fixed crush with sequence update
  * Added proper support for strand-specific reverse protocol 


Version 0.5.3 - Jan 15, 2014
----------------------------
 
  * Fixed major bug in local alignment analysis
  * Major update for simulation pipeline


Version 0.5.2 - Jan 7, 2014
---------------------------
 
  * Fixed major bug with unique partners in clustering
  * Fixed bug with same breakpoints from rescued candidates
  * Added option to merge intersecting clusters


Version 0.5.1 - Dec 17, 2013
----------------------------
 
  * Fixed bug with rescuing clipped alignments
  * Added sequence offset for paired-only clusters 
  * Added new option for homogeneity weight
  * Added alignment score for breakpoint candidates
  * Boost is linked as static library from now on


Version 0.5 - Dec 4, 2013
-------------------------
 
  * Improved external binaries detection 
  * Fixd biotype based filter
  * Removed boost regex dependency
  * setup_reference_dataset: fix bug with Ensembl version, add download of repeats track
  * Added number of scripts for comparison of fusions
  * Added new filtering options and fusion type detection


Version 0.4.1 - Oct 18,2013
---------------------------
 
  * Added  search for discordant pairs from genomic alignments
  * Added suport for genomic alignments when search for mates of splitted reads


Version 0.4 - Oct 15,2013
-------------------------
 
  * Homology detection added for fusions originating from introns and intergenic regions
  * Several updates in reports
  * Additional clippling regions size for local alignments is now configurable
  * Added metacluster homogeneity analysis 
  * Added split position based filtering


Version 0.3 - Oct 3,2013
-------------------------
 
  * Joining of fusions separted by intron is added as an explicit pipeline step
  * Improved joining algorithm significantly


Version 0.2.2 - Sep 26,2013
----------------------------
 
  * Added separation of clusters by breakpoint position during clustering
  * Integerated fusion filtering based on unique alignment rate
  
     
Version 0.2.1 - Sep 21,2013
----------------------------
 
  * Added bowtie2 based homologous genes detection
  * Added number of unique read starts to report
  * Fixed reads rescue for paired-only clusters


Version 0.2 - Sep 12,2013
--------------------------
    
  * Decreased memory consumption
  * Added strandness annotation
  * Added insert size based fusion filtering
  * Optimized rescue alignments


Version 0.1.2 - Aug 27,2013
----------------------------
    
  * New algorithm for clustering based on ClusterTree structure  


Version 0.1.1 - Aug 27,2013
----------------------------
    
  * Added fusions in non-coding regions  
  * Fixed single-end related problems
  * Added initial fusion post-processing
  * Fixed multiple minor bugs 


Version 0.1alpha - July 16,2013
-------------------------------

  * First public release


