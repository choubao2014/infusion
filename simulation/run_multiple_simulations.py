import argparse
import random
import os
import time
import shutil
import matplotlib.pyplot as plt
import numpy as np
import logging
import datetime
import sys

dirpath = os.path.dirname(__file__)
sys.path.append( os.path.join(dirpath, "..") )

from fusion import getColorsList

# simulation command

SIMULATE_FUSIONS_CMD="%s %d %s"

# scripts for simulation

CREATE_TRANSCRIPTS_CMD="simulation/getTranscriptsFromFusimDesign.py -d %s -r %s -o %s"

SIMULATE_READS_CMD="simulation/simulateRNASeqWithFusions.py -ft %s -s %d -o %s"

SHOW_EVIDENCE_CMD="simulation/showEvidenceForFusimDesign.py -d %s -b > %s/validated.txt"

OUTPUT_SUPPORTERS_CMD="simulation/showEvidenceForFusimDesign.py -d %s -a %s -r > %s/designed_supporters.txt"

# detect fusions command

ANALYZE_SIMULATION_CMD="%s %s %s %s"

# compare simulations script

COMPARE_CMD="comparison/run_comparison.py --roc %s"


#

NON_STRAND_SPECFIC = 0
STRAND_FORWARD = 1
STRAND_REVERSE = 2


class Config:

    def __init__(self):
        self.opts = {}
        self.infusionDirPath = ""
        self.simulateFusionsScriptPath = ""
        self.detectFusionsScriptPath = ""
        self.comparisonConfigurationTemplate = ""
        self.pathToGenome = ""
        self.readSize = 75
        self.insertSize = 300
        self.strandSpecificity = NON_STRAND_SPECFIC


    def loadConfigurationFile(self,filepath):
        self.opts = {}

        # load configuration file

        cfg = open(filepath)
        for line in cfg:
            if len(line.strip()) == 0 or line.startswith("#"):
                continue
            else:
                items = line.split("=")
                if len(items) != 2:
                    logger.error( "ERROR! Incorrect config line:\n%s" % line )
                    sys.exit(-1)
                else:
                    self.opts[ items[0].strip() ] = items[1].strip()


        # check if all required configuration items are provided

        for optName, optVal in self.opts.iteritems():
            if optName == "INFUSION_DIR":
                self.infusionDirPath = optVal
            elif optName == "SIMULATE_FUSIONS_SCRIPT":
                self.simulateFusionsScriptPath = optVal
            elif optName == "DETECT_FUSIONS_SCRIPT":
                self.detectFusionsScriptPath = optVal
            elif optName == "COMPARISON_CONFIGURATION_TEMPLATE":
                self.comparisonConfigurationTemplate = optVal
            elif optName == "GENOME_FASTA":
                self.pathToGenome = optVal
            elif optName == "READ_SIZE":
                self.readSize = int(optVal)
            elif optName == "INSERT_SIZE":
                self.insertSize = int(optVal)
            elif optName == "STRAND_SPECIFICITY":
                if optVal == "forward":
                    self.strandSpecificity = STRAND_FORWARD
                elif optVal == "reverse":
                    self.strandSpecificity = STRAND_REVERSE
                else:
                    logger.warning("WARNING! Unknown strand specificity %s! Non-strand-specific mode applied.cd")
            else:
                logger.warn("WARNING! Unknown configuration option: " + optName)

    def checkOptions(self):

        if not os.path.exists(self.infusionDirPath):
            logger.error("ERROR! Configuration file error! Infusion path %s does not exist!" % self.infusionDirPath)
            sys.exit(-1)

        if not os.path.exists(self.simulateFusionsScriptPath):
            logger.error("ERROR! Configuration file error! Simulate fusions script %s is not found!"
                         % self.simulateFusionsScriptPath)
            sys.exit(-1)

        if not os.path.exists(self.detectFusionsScriptPath):
            logger.error("ERROR! Configuration file error! Detect fusions script %s is not found!"
                         % self.detectFusionsScriptPath)
            sys.exit(-1)

        if not os.path.exists(self.comparisonConfigurationTemplate):
            logger.error("ERROR! Configuration file error! Comparison configuration template %s is not found!"
                         % self.comparisonConfigurationTemplate)
            sys.exit(-1)

        if not os.path.exists(cfg.pathToGenome):
            logger.error("ERROR! Genome FASTA file %s is not found" % cfg.pathToGenome)
            sys.exit(-1)

        if cfg.readSize <= 0:
            logger.error("ERROR! Read size %d  is not accepted!" % cfg.readSize)


    def getInFusionPythonCmd(self, args):
        return "python " + self.infusionDirPath + "/" + args


def loadResults(datasets, path):
    
    tool_name = ""
    for line in open(path):
        if line.startswith(">"):
            tool_name = line.strip()[1:]
        else:
            dataset = datasets.get(tool_name, {} )
            items = line.strip().split()
            assert len(items) == 7
            score = int(items[0])
            tp = int(items[1])
            fp = int(items[2])
            total = int(items[3])
            recall = float(items[4])
            precision = float(items[5])
            fmeasure = float(items[6])
            val_row = dataset.get(score, [ [] ,[], [], [], [], [] ] )
            vec = (tp,fp,total,recall,precision,fmeasure) 
            for i in range(len(vec)):
                val_row[i].append(  vec[i] )
            dataset[score] = val_row
            datasets[tool_name] = dataset
            
def plotToolData(tool_datasets, idx,  x_label, y_label, title, snPath):
    fig, ax = plt.subplots()

    colors = getColorsList()

    index = 0


    for tool_name in tool_datasets:
        x = []
        y = []

        #print "Drawing for", tool_name
        dataset = tool_datasets[tool_name]
        for score, data in dataset.iteritems():
            
            x.append( score )
            y.append( data[idx] )

        ax.plot(x, y, 'k', label=tool_name, color=colors[index])
        index += 1

    # Now add the legend with some customizations.
    legend = ax.legend(loc='lower right', shadow=True)

    # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame  = legend.get_frame()
    frame.set_facecolor('0.90')

    # Set the fontsize
    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)

    plt.savefig(snPath, format="SVG")

def sortedToolNames(names):
    if "InFusion" in names:
        idx = names.index("InFusion")
        new_names = [ "InFusion" ] + names[0:idx] + names[idx+1:]
        return new_names
    else:
        return names

def plot2Datasets(tool_datasets, idxs, x_label, titles, path):

    fig = plt.figure(figsize=(12, 5), dpi=100,)
    fig.subplots_adjust(hspace=1.5)

    #fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 5), dpi=100)
    #fig.tight_layout()

    axes = []
    axes.append( plt.subplot(121) )
    axes.append(  plt.subplot(122) )

    fig.tight_layout(pad=3.5)

    colors = getColorsList()

    for i in range(len(idxs)):
        y_idx = idxs[i]
        ax = axes[i]
        color_idx = 0

        tool_names = sortedToolNames( tool_datasets.keys() )
        for tool_name in tool_names:
            x = []
            y = []

            dataset = tool_datasets[tool_name]
            for score, data in dataset.iteritems():
            
                x.append( score )
                idx = idxs[i]
                y.append( data[idx] )

            ax.plot(x, y, 'k', label=tool_name, color = colors[color_idx])
            color_idx += 1

        if i == 1:
            #legend = ax.legend(loc='upper left', shadow=True)
        #else:
            legend = ax.legend(loc='lower right', shadow=True)

            frame  = legend.get_frame()
            frame.set_facecolor('0.90')

            # Set the fontsize
            for label in legend.get_texts():
                label.set_fontsize('medium')

            for label in legend.get_lines():
                label.set_linewidth(1.5)  # the legend line width

        ax.set_xlabel(x_label)
        ax.set_ylim(0,1.0)
        ax.set_title(titles[i])
        ax.grid()

    plt.savefig(path, format="SVG")



def plotPRCurve(tool_datasets, title, path):
    fig, ax = plt.subplots()

    colors = getColorsList()

    index = 0

    x_label = "Recall"
    y_label = "Precision"

    for tool_name in tool_datasets:
        x = []
        y = []

        #print "Drawing for", tool_name
        dataset = tool_datasets[tool_name]
        for score, data in dataset.iteritems():
            
            x.append( data[3] )
            y.append( data[4] )

        ax.plot(x, y, 'k', label=tool_name, color = colors[index])
        index += 1

    # Now add the legend with some customizations.
    legend = ax.legend(loc='lower right', shadow=True)

    # The frame is matplotlib.patches.Rectangle instance surrounding the legend.
    frame  = legend.get_frame()
    frame.set_facecolor('0.90')

    # Set the fontsize
    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plt.title(title)

    plt.savefig(path)

def plotBpPrecision(data, title, path):
    

    idx = 0

    bins = []
    for i in range(-11,11,1):
        bins.append( i + 0.5)


    for tool_name in data:

        bp_precision = data[tool_name]

        fig, ax = plt.subplots()

        #n, bins, patches = ax.hist([bp_precision], 11, normed=True, histtype='step', label=tool_name)

        n, bins, patches = ax.hist(bp_precision, bins=bins,  normed=True, histtype='bar', fill=True, rwidth=0.8,
                                   label=tool_name)

        idx +=1


        legend = ax.legend(loc='upper right', shadow=True)
        
        plt.title(title)
        plt.title("N=%d, mean=%.1f std=%.1f median=%.1f " %
                  (len(bp_precision), np.mean(bp_precision), np.std(bp_precision), np.median(bp_precision)) )
        plt.xlabel("Breakpoint deviation (bp)")
        savePath = path.replace(".png", "_%s.png" % tool_name)
        plt.savefig(savePath)




def setupLogger(logfile_path):
    
    logging.basicConfig(level=logging.DEBUG,
                      format='%(asctime)s [%(levelname)s] %(message)s\n',
                      datefmt='%m-%d %H:%M',
                      filename=logfile_path,
                      filemode='w')
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter('%(asctime)s %(message)s', datefmt='%H:%M:%S') )
    logger = logging.getLogger('')
    logger.addHandler(console)
    
    return logger


def catAndZip(fusion_reads, bg_reads, out_reads):
    
    logger = logging.getLogger('')
    
    assert os.path.exists(fusion_reads), "File doesn't exist %s" % fusion_reads  
    assert os.path.exists(bg_reads), "File doesn't exist %s" % bg_reads

    cmd_args = ["cat", fusion_reads, bg_reads, "| gzip >", out_reads]

    cmd = " ".join(cmd_args)
    logger.debug(cmd)
    os.system(cmd)

    assert os.path.exists(out_reads)
    #os.path.rm(fusion_reads)
    

 

def concatenateReads(run_dir, bg_reads_dir):
    
    out_reads = ["reads_with_bg_1.fq.gz", "reads_with_bg_2.fq.gz"]

    # read files name assumed reads_1.fq, reads_2.fq
    
    fusion_reads_1 = os.path.join(run_dir, "reads_1.fq")
    bg_reads_1 = os.path.join(bg_reads_dir, "reads_1.fq")
    out_reads_1 = os.path.join(run_dir, out_reads[0])
    
    if not os.path.exists(out_reads_1):
        catAndZip(fusion_reads_1, bg_reads_1, out_reads_1)

    fusion_reads_2 = os.path.join(run_dir, "reads_2.fq")
    bg_reads_2 = os.path.join(bg_reads_dir, "reads_2.fq")
    out_reads_2 = os.path.join(run_dir, out_reads[1])

    if not os.path.exists(out_reads_2):
        catAndZip(fusion_reads_2, bg_reads_2, out_reads_2)

    if os.path.exists(fusion_reads_1):
        os.remove(fusion_reads_1)

    if os.path.exists(fusion_reads_2):
        os.remove(fusion_reads_2)

    return out_reads
    
def splitValidated(path_to_validated, num_groups):
    
    lines = []
    for line in open(path_to_validated):
        if line.startswith("#"):
            continue

        lines.append( line)


    step_size = len(lines) / num_groups
    dirpath = os.path.dirname(path_to_validated)

    vpaths = []
    count = 0
    out_file = None
    for i in range(0,len(lines)):
        if i % step_size == 0:
            if out_file:
                out_file.close()
            count += 1
            path = os.path.join(dirpath, "validated_%d.txt" % count)
            out_file = open(path, "w")
            vpaths.append(path)
        out_file.write(lines[i])
            
    if out_file:
        out_file.close()

    return vpaths

def loadBpPrecision(bp_precision, path):
    name = ""
    for line in open(path):
        if line.startswith(">"):
            items = line.strip().split()
            #if len(items) != 3:
            #    continue
            name = items[0][1:]
            continue

        pr_array = bp_precision.get(name, [])

        dists = line.strip().split(":")
        assert(len(dists) == 2)

        break_size = int(dists[0])
        num_breaks = int(dists[1])

        #pr_array[int(dists[0])] += int(dists[1])
        for i in range(0, num_breaks):
            pr_array.append(break_size)

        bp_precision[name] = pr_array


def checkReport(reportFile):

    warningOn = False
    for line in open(reportFile):
        if line.startswith("WARNINGS"):
            warningOn = True
            continue

        if warningOn:
            if len(line.strip()) == 0:
                warningOn = False
            else:
                logger.warning(line.strip())



def summarizeAndPlotResults(results, group_name, outDir):
    
    datasets = {}
    bp_precision_set = {}
    num_samples = 0
    for r in results:
        logger.debug( "Loading %s" % r )
        loadResults(datasets, r)

        cmp_results_dir = os.path.dirname(r)
        report_path =os.path.join(cmp_results_dir, "report.txt")

        if os.path.exists(report_path):
            logger.debug("Checking main report from %s" % report_path)
            checkReport(report_path)

        pr_path = os.path.join(cmp_results_dir, "bp_precision.txt")
        if os.path.exists(pr_path):
            loadBpPrecision(bp_precision_set, pr_path )

        num_samples += 1
    
    # normalize

    stds = {}

    for tool_name, dataset in datasets.iteritems():
        
        scores = dataset.keys()
        for score in scores:
            val_row = dataset[score]
            val = [ np.mean(x) for x in val_row]
            dataset[score] = val
            if score == max_score:
                stds[tool_name] = [ np.std(x) for x in val_row ]



    data_file = open("%s/%s_sim_data.txt" % (outDir,group_name) , "w")
    final_report = open("%s/%s_report.txt" % (outDir,group_name) , "w")
    tex_report = open("%s/%s_latex_report.txt" % (outDir,group_name), "w")
    
    final_report.write("#Num samples = %d\n" % num_samples )
    final_report.write("Tool\tTP\tFP\tTOTAL\tRECALL\tPRECISION\tF-MEASURE\n")

    tex_report.write("\hline\n")
    tex_report.write("\\textit{Tool} & \\textit{N} & \\textit{TP} & \\textit{FP} & \\textit{RECALL} & \\textit{PRECISION} \\\\\n")
    tex_report.write("\hline\n")


    for tool_name, dataset in datasets.iteritems():
        data_file.write(">%s\n" % tool_name) 
        for score, val in dataset.iteritems():
            #data_file.write( "%d:%d\t%d:%d\t%d:%d\t%d:%d\t%f\t%f\t%f\n" % (score, val[0], val[1], val[2], val[3], val[4], val[5] )  )
            if score == max_score:

                std = stds[tool_name]

                #final_report.write("%s\t%d\t%d\t%d\t%.2f\t%.2f\t%.2f\n" % (tool_name, val[0],val[1], val[2], val[3], val[4], val[5] ))
                final_report.write("%s\t%d:%d" % (tool_name, val[0], std[0]) )
                final_report.write("\t%d:%d" % (val[1], std[1]) )
                final_report.write("\t%d:%d" % (val[2], std[2]) )
                final_report.write("\t%.2f:%.2f" % (val[3], std[3]) )
                final_report.write("\t%.2f:%.2f" % (val[4], std[4]) )
                final_report.write("\t%.2f:%.2f\n" % (val[5], std[5]) )

                tex_report.write("%s & %d $\pm$ %d" % (tool_name, val[2], std[2]) )
                tex_report.write(" & %d $\pm$ %d" % (val[0], std[0]) )
                tex_report.write(" & %d $\pm$ %d" % (val[1], std[1]) )
                tex_report.write(" & %.2f $\pm$ %.2f" % (val[3], std[3]) )
                tex_report.write(" & %.2f $\pm$ %.2f \\\\\n" % (val[4], std[4]) )
                tex_report.write("\hline\n")


    if bp_precision_set:
        bp_report_file = open("%s/%s_bp_precision.txt" % (outDir,group_name) , "w")
        bp_report_file.write("Tool\tNum valid fusions\tBreakpoint precision\nBreakpoint shift (mean)\tBreakpoint shift (std)\n")
        for tool in bp_precision_set:
            bp_set = bp_precision_set[tool]
            bp_mean = np.mean(bp_set)
            bp_std = np.std(bp_set)

            exactBp = 0
            for bp in bp_set:
                if bp == 0:
                    exactBp += 1
            mean_bp_precision = float(exactBp) / len(bp_set) #TODO: devide by num real?
            bp_report_file.write("%s\t%d\t%.2f\t%.2f\t%.2f\n" % (tool, len(bp_set), mean_bp_precision, bp_mean, bp_std) )
        bp_report_file.close()

    #snPath = os.path.join(outDir, "%s_recall.png" % group_name )
    #plotToolData(datasets, 3, "Number of supporting reads", "Recall","Recall comparison (%d simulations) " % num_samples, snPath)

    #fdrPath = os.path.join(outDir, "%s_precision.png" % group_name)
    #plotToolData(datasets, 4,  "Number of supporting reads", "Precision", "Precision comparison (%d simulations)" % num_samples, fdrPath)
    
    fmPath = os.path.join(outDir, "%s_fmeasure.svg" % group_name )
    plotToolData(datasets, 5,  "Number of supporting reads", "F-measure", "F-measure comparison (%d simulations)" % num_samples, fmPath)

    #if bp_precision:
        #bpPresPath = os.path.join(outDir, "%s_bp_precision.png" % group_name )
        #plotBpPrecision(bp_precision, "Breakpoint precision (%d simulations)" % num_samples, bpPresPath)

    bothPlotsPath = os.path.join(outDir, "%s_recall_and_precision.svg" % group_name)
    plot2Datasets(datasets, [3,4],  "Number of supporting reads", ["Recall", "Precision"], bothPlotsPath)


def distributeFusionsInGroups(fusion_paths, fusions_file):

    num_groups = len(fusion_paths)
    fusion_annotations = []

    for line in open(fusions_file):
        fusion_annotations.append(line)

    num_fusions = len(fusion_annotations)
    num_fusions_per_group = num_fusions / num_groups
    for i in range(num_groups):

        start_idx = i*num_fusions_per_group
        end_idx = (i+1)*num_fusions_per_group
        out_file = open(fusion_paths[i], "w" )
        for j in range(start_idx, end_idx):
            line = fusion_annotations[j]
            print "Writing ", line
            out_file.write(line)


def setupFusionGroups(fusions_dir, num_groups):

    fusion_paths = []
    transcript_paths = []
    run_dirs = []

    if not os.path.exists(fusions_dir):
        os.mkdir(fusions_dir)

    for i in range(num_groups):

        group_name = "group_%02d" % (i + 1)
        fusions_file = "fusions_%02d.txt" % (i+1)
        transcripts_file = "fusions_%02d.fa" % (i+1)

        run_dir = os.path.join( fusions_dir, group_name )
        run_dirs.append( run_dir )
        fusion_paths.append( os.path.join( fusions_dir, fusions_file ) )
        transcript_paths.append( os.path.join(fusions_dir, transcripts_file) )

    return fusion_paths, transcript_paths, run_dirs




if __name__ == "__main__":

    descriptionText = "The script creates simulation from existing runs"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("-s", dest="seed", default=42, type=int, help="Seed for random generator")
    
    parser.add_argument("-n", dest="num_simulations", required="true", type=int,
                        help="Number of simulations with the same designed")

    parser.add_argument("-c", action="store", dest="configuration_file", required = "true",
                        help="Path to configuration file of simulation" )

    parser.add_argument("--num-sim-to-skip", action="store", default=0, dest="num_to_skip", type=int,
                        help="Number of simulations to skip at start.")

    parser.add_argument("--num-groups", action="store", default=1, dest="num_groups", type=int,
                        help="Number of groups for per group comparison.")

    parser.add_argument("--skip-simulation", action="store_true", dest="skip_simulation", default=False,
                        help="Skip simulation of fusions")

    parser.add_argument("--skip-discovery", action="store_true", dest="skip_discovery", default=False,
                        help="Skip detection of fusions from simulation run")

    parser.add_argument("--skip-comparison", action="store_true", dest="skip_comparison", default=False,
                        help="Skip comparison of fusions")
    
    parser.add_argument("--skip-analysis", action="store_true", dest="skip_analysis", default=False,
                        help="Skip analysis of fusions")

    parser.add_argument("-bg", action="store", dest="bg_reads_dir", default="",
                        help="Path to dir with background reads. If provided will be included into simulation experiment")

    parser.add_argument("--bp-precision", action="store_true", dest="bp_precision", default=False,
                        help="Analyze computed breakpoint position divergence during simulation")


    args = parser.parse_args()
    
    random.seed(args.seed)
    
    localtime   = time.localtime()
    time_str  = time.strftime("%Y-%m-%d-%H-%M", localtime)
    report_dir = "report_" + time.strftime("%Y_%m_%d_%H%M")
    logfile_path = "simulation.%s.log" % time_str
    logger = setupLogger(logfile_path)
    logger.debug("Started simulation experiment")

    logger.debug("Options are\n%s", args)

    cfg = Config()

    logger.debug("Loading configuration file %s\n", args.configuration_file)
    cfg.loadConfigurationFile(args.configuration_file)

    cfg.checkOptions()
    logger.debug("Configuration options:\n" + str(cfg.opts))
    logger.debug("Configuration checked.")



    if not args.skip_discovery:
        if cfg.detectFusionsScriptPath == 0 or not os.path.exists(cfg.detectFusionsScriptPath):
            logger.error("Error! Path to detect fusions script file is not provided correctly.")
            sys.exit(-1)

    result_set = []

    if not args.skip_analysis:
        for i in range(args.num_groups):
            result_set.append( [] )


    for i in range(args.num_simulations):
        
        t0 = time.time()

        run_seed = random.randint(1,100000)

        if i < args.num_to_skip:
            continue

        fusions_file = os.path.abspath("./fusions_%02d.txt" % (i+1))

        if args.num_groups > 1:
            dir_name = os.path.abspath("./set_%02d" % (i + 1))
            fusion_paths,transcript_paths,run_dirs = setupFusionGroups(dir_name, args.num_groups)
        else:
            run_dirs = [os.path.abspath("./set_%02d" % (i + 1) )]
            fusion_paths = [os.path.abspath("./fusions_%02d.txt" % (i+1)) ]
            transcript_paths = [os.path.abspath("./fusions_%02d.fa" % (i+1)) ]

        if not args.skip_simulation:

            logger.debug("Starting simulation %d" % (i+1) )
            
            if not os.path.exists(fusions_file):

                simFusionsScriptPath = cfg.simulateFusionsScriptPath

                simulate_fusions_cmd =  SIMULATE_FUSIONS_CMD % (simFusionsScriptPath, run_seed, fusions_file)
                logger.debug( simulate_fusions_cmd + "\n" )
                os.system(simulate_fusions_cmd)
            else:
                logger.debug("Fusions file %s exists already, skipping this step..." % fusions_file)

            if args.num_groups > 1:
                distributeFusionsInGroups(fusion_paths, fusions_file)

            for i in range(args.num_groups):
                fusions_file = fusion_paths[i]
                transcripts_file = transcript_paths[i]
                run_dir = run_dirs[i]

                if os.path.exists(transcripts_file):
                    logger.debug("Fusion transcripts file %s already exists, skipping this step..." % transcripts_file)
                else:
                    create_transcripts_script = CREATE_TRANSCRIPTS_CMD % (fusions_file, cfg.pathToGenome, transcripts_file)
                    create_transcripts_cmd = cfg.getInFusionPythonCmd( create_transcripts_script )
                    logger.debug( create_transcripts_cmd + "\n")
                    os.system(create_transcripts_cmd)

                simulate_reads_script = SIMULATE_READS_CMD % (transcripts_file, run_seed, run_dir)
                simulate_reads_script += " -L %s " % cfg.readSize
                simulate_reads_script += " -l %s " % cfg.insertSize
                simulate_reads_script += " -p %s " % cfg.strandSpecificity
                simulate_reads_script += fusions_file
                simulate_reads_cmd = cfg.getInFusionPythonCmd(simulate_reads_script)
                logger.debug(simulate_reads_cmd + "\n")
                os.system(simulate_reads_cmd)
       
                show_ev_cmd = cfg.getInFusionPythonCmd( SHOW_EVIDENCE_CMD % (fusions_file, run_dir) )
                logger.debug(show_ev_cmd + "\n")
                os.system(show_ev_cmd)

                aln_file = os.path.join(run_dir, "reads_alignment.sam")
                output_supporters_script = OUTPUT_SUPPORTERS_CMD % (fusions_file, aln_file, run_dir)
                output_supporters_cmd = cfg.getInFusionPythonCmd(output_supporters_script)
                logger.debug(output_supporters_cmd)
                os.system(output_supporters_cmd)


        else:
            logger.debug( "Simulation %d skipped" % (i+1) )


        if not args.skip_discovery:
            logger.debug("Starting fusion discovery...")
            for run_dir in run_dirs:

                if args.bg_reads_dir:
                    read_names = concatenateReads(run_dir, args.bg_reads_dir)
                else:
                    read_names = ["reads_1.fq", "reads_2.fq"]

                logger.debug("Launching fusion detection tools...")

                analyze_cmd = ANALYZE_SIMULATION_CMD % (cfg.detectFusionsScriptPath, run_dir, read_names[0], read_names[1] )
                logger.debug( analyze_cmd )
                os.system(analyze_cmd)
        else:
            logger.debug( "Fusion discovery skipped" )


        group_results = []

        if not args.skip_comparison:
            logger.debug("Starting comparison...")
            configTemplatePath = cfg.comparisonConfigurationTemplate
            if not os.path.exists(configTemplatePath):
                logger.error("ERROR: comparison configuration %s file is not found!" % configTemplatePath)
                sys.exit(-1)

            for run_dir in run_dirs:
                config_path = os.path.join(run_dir, os.path.basename(configTemplatePath))
                logger.debug("Copying comparison configuration %s to %s..\n" % (configTemplatePath, config_path))
                shutil.copy(cfg.comparisonConfigurationTemplate, config_path)
                if args.bp_precision:
                    config_path = "--bp-precision " + config_path
                compare_cmd = cfg.getInFusionPythonCmd( COMPARE_CMD % (config_path) )

                logger.debug( compare_cmd + "\n" )
                os.system(compare_cmd)

        else:
            logger.debug( "Comparison skipped" )
        
        if not args.skip_analysis:
            logger.debug("Starting analysis...\n")
            for i in range(args.num_groups):
                sim_dir = run_dirs[i]
                res_dir_name = os.path.basename(cfg.comparisonConfigurationTemplate).replace(".cfg","_results")
                result_set[i].append( os.path.join(sim_dir, res_dir_name, "plot_data.txt"))
        else:
            logger.debug("Analysis skipped")

        t1 = time.time()
        logger.debug( "Simulation round time: %s" % str(datetime.timedelta(seconds=(long(t1-t0)))) )

    # what is this?
    max_score = 149
    
    if result_set:
        logger.debug( "Analyzing final results\n" )

        if not os.path.exists(report_dir):
            os.mkdir(report_dir)

        if args.num_groups > 1:
            for i in range(args.num_groups):
                summarizeAndPlotResults(result_set[i], "group%d" % (i+1), report_dir )
        else:
            summarizeAndPlotResults(result_set[0], "total", report_dir)


    logger.debug("Finished simulation experiment")

    # move log file
    if os.path.exists(report_dir):
        novel_logfile_path = os.path.join(report_dir, os.path.basename(logfile_path))
        shutil.move(logfile_path, novel_logfile_path)



        
        

