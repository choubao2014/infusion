#!/usr/bin/python

import re
import sys
import argparse
import os.path
import HTSeq

    
if __name__ == "__main__":
    
    descriptionText = "The script converts UCSC BED file to GTF format."
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("-i", action="store", required="true", dest="gtfFile", help="Input file with list of genes in GTF format")
    #parser.add_argument("-o", action="store", dest="outFile", default="", help="Output file. Default is output.fa")
    
    parser.add_argument("--add-chr-prefix", action="store_true", dest="addPrefix", help="Add \"chr\" prefix to the name of the chromosome.")
    parser.add_argument("--use-names", action="store_true", dest="useNames", help="Use transcript names instead of IDs.")

    args = parser.parse_args()
    
    #print args

    attrName = 'transcript_id'
    if args.useNames:
        attrName = 'transcript_name'

    gtfFileName = args.gtfFile
    #outFileName = args.outFile
    
    #outFileName = gtfFileName.replace(".gtf", ".new.bed")

    #outFile = open(outFileName, "w")


    gtf_file = HTSeq.GFF_Reader( gtfFileName )

    transcripts = {}
    
    for feature in gtf_file:
        if feature.type == 'exon':
            transcriptName = feature.attr[ attrName ]
            if transcriptName in transcripts:
                transcripts[transcriptName].append(feature)
            else:
                transcripts[transcriptName] = [feature]
    
    #print "Processed GTF file. Writng output...\n"

    for tName in transcripts:
        tr = transcripts[tName]
        starts = []
        sizes = []
        for exon in tr:
            #print exon
            # print exon.iv.start
            starts.append( exon.iv.start )
            sizes.append( exon.iv.end - exon.iv.start )
        seqName = tr[0].iv.chrom
        if not seqName.startswith("chr") and args.addPrefix:
            seqName = "chr" + seqName
        
        strand = tr[0].iv.strand
        startPos = min(tr[0].iv.start,tr[-1].iv.start)
        endPos = max(tr[-1].iv.end,tr[0].iv.end)
        
        if tr[0].iv.start > tr[-1].iv.start:
            starts.reverse()
            sizes.reverse()
        
        sizesStr = ""
        for s in sizes:
            sizesStr += str(s) + ","
        
        offsetStr = ""
        for p in starts:
            offsetStr += str(p - startPos) + ","
        
        print "%s\t%d\t%d\t%s\t0\t%s\t%d\t%d\t0\t%d\t%s\t%s" % (seqName,startPos,endPos,tName,strand,startPos,endPos,len(tr),sizesStr,offsetStr)
     
  
