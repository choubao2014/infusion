import argparse
from Bio import SeqIO
import os.path
import HTSeq

from simlib import *


def loadTranscripts(gtfFileName):

    attrName = "transcript_id"

    gtf_file = HTSeq.GFF_Reader( gtfFileName )

    transcripts = {}

    for feature in gtf_file:
        if feature.type == 'exon':
            transcriptName = feature.attr[ attrName ]
            if transcriptName in transcripts:
                transcripts[transcriptName].append(feature)
            else:
                transcripts[transcriptName] = [feature]

    return transcripts


def writeSequence(outputFileName, seqName, seq):
    outFile = open(outputFileName, "w")
    fileHeader = ">%s\n" % seqName
    outFile.write(fileHeader)
    outFile.write(str(seq) + "\n")
    outFile.close()


def prepareOutputFolder(outDir):
    if os.path.exists(outDir):
        print "Warning! Output dir already exist, contents will be rewritten..."
        pass
    else:
        os.mkdir(outDir)

    return os.path.abspath(outDir)

UPSTREAM_OFFSET = 500
DOWNSTREAM_OFFSET = 500

def parseFusions(transcripts):
    fusionPartExons = []
    transcript = transcripts[fPart.transcriptId]
    startPos = -1
    endPos = -1
    chrom = ""
    startCollectingExons = False
    strand = '.'
    for exon in transcript:

        exonId = exon.attr["exon_number"]
        if exonId == fPart.exonStartId:
            strand = exon.iv.strand
            if strand == '+':
                startPos = exon.iv.start
            else:
                endPos = exon.iv.end
            chrom = exon.iv.chrom
            startCollectingExons = True

        if exonId == fPart.exonEndId:
            if strand == '+':
                endPos = exon.iv.end
            else:
                startPos = exon.iv.start
            fusionPartExons.append(exon)
            break

        if startCollectingExons:
            fusionPartExons.append(exon)

if __name__ == "__main__":
    
    descriptionText = "The script creates artificial fusion chromosomes with annotations based on given simulation design file"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("-d", action="store", required="true", dest="fusionsFile",
                        help="input file with simulated fusions in Fusim-like format")
    #parser.add_argument("-g", action="store", required="true", dest="gtfFile",
    #    help="input file with list of genes in Ensembl GTF format")
    parser.add_argument("-f", action="store", required="true", dest="fastaFile",
        help="input genome sequences in FASTA format")
    parser.add_argument("-o", action="store", required="true", dest="outDir", help="path to output folder")
    parser.add_argument("--output-exons", action="store_true", default="False",
                        dest="outputExons", help="Output additional FASTA file containing only exons ")


    args = parser.parse_args()
    
    print args

    designedFusions = parseFusionsDesign(args.fusionsFile)

    print "Loading transcripts..."
    #transcripts = loadTranscripts(args.gtfFile)

    print "Loading sequences..."
    seqData = SeqIO.to_dict(SeqIO.parse(args.fastaFile, "fasta"))

    print "Preparing output folder..."
    outDirPath = prepareOutputFolder(args.outDir)

    print "Generating fused chromosomes"

    gtfLines = []
    count = 1

    for fusion in designedFusions:

        fusionName = fusion.name

        fusionChromosomeName =  "fusedChr%d" % count
        fusionGeneName = "fusionGene%d" % count
        fusionChromosomeSeq = ""
        offset = 0
        exon_count = 1

        for fPart in fusion.parts:
            fusionPartExons = []
            #transcript = transcripts[fPart.transcriptId]

            startPos = fPart.exonStarts[0]
            endPos = fPart.exonEnds[-1]
            numExons = len(fPart.exonStarts)

            assert startPos != -1
            assert endPos != -1


            startPos -= UPSTREAM_OFFSET
            if startPos < 0:
                startPos = 0

            endPos += DOWNSTREAM_OFFSET

            buf = seqData[ fPart.seqName ].seq[ startPos : endPos  ]
            totalLen = len(buf)

            if fPart.strand == '-':
                buf = buf.reverse_complement()
            fusionChromosomeSeq += buf

            for i in range(0, numExons):
                line = "%s\t" % fusionChromosomeName
                line += "%s\t" % "InFusion"
                line += "%s\t" % "fusion_gene"
                if fPart.strand == '+':
                    exonStartPos = fPart.exonStarts[i] - startPos
                    exonEndPos = fPart.exonEnds[i] - startPos
                else:
                    exonEndPos = totalLen - (fPart.exonStarts[i]  - startPos ) + 1
                    exonStartPos = totalLen - (fPart.exonEnds[i] - startPos ) + 1
                line += "%d\t" % (offset + exonStartPos)
                line += "%d\t" % (offset + exonEndPos )
                line += ".\t+\t.\t"
                line += "gene_id \"%s\"; transcript_id \"%s\";" % (fusionGeneName, fusionName)
                line += " exon_number \"%d\";" % exon_count
                line += " gene_name \"%s\"; transcript_name \"%s\";\n" % (fusionGeneName, fusionName)
                exon_count += 1
                gtfLines.append(line)

            offset += totalLen

        outputFileName = os.path.join(outDirPath,fusionChromosomeName+".fa" )
        writeSequence(outputFileName, fusionChromosomeName, fusionChromosomeSeq)
        count += 1

    for seqName, seqRec in seqData.items():
        outFileName = os.path.join(outDirPath, seqName+".fa")
        writeSequence(outFileName, seqName, seqRec.seq)

    outGtf = open(os.path.join(outDirPath, "fusions.gtf"), "w")
    outGtf.write("# generated by infusion simulation pipeline\n")
    for line in gtfLines:
        outGtf.write(line)



