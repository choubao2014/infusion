__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import argparse
import sys
import os
import tempfile

from collections import defaultdict

import HTSeq
from Bio.Blast.Applications import NcbiblastnCommandline
from Bio.Blast import NCBIXML

FORWARD_STRAND = '+'
REVERSE_STRAND = '-'
UNKNOWN_STRAND = '.'

FUSION_DIRECTION_UNKNOWN = '^'
FUSION_DIRECTION_UPSTREAM = '<'
FUSION_DIRECTION_DOWNSTREAM = '>'

BP_SPAN = 'local'
BP_BRIDGE_PAIR = 'bridge_pair'


def getColorsList():
    colors = [(1, 0, 0),
              (0, 1, 0),
              (0, 0, 1),
              (0.75, 0.75, 0),
              (0.0, 0.75, 0.75),
              (0.75, 0.0, 0.75),
              (1, 0.5, 0) ]

    return colors

class FusionInterval:
    def __init__(self, fusionIdx, idx):
        self.fusionId = fusionIdx
        self.id = idx
        self.ref = ""
        self.bpos = -1
        self.startPos = -1
        self.endPos = -1
        self.seq = ""
        self.dir = FUSION_DIRECTION_UNKNOWN

    def __len__(self):
        return self.endPos - self.startPos + 1
    
    def __str__(self):
        return "%s:%d" % (self.ref,self.bpos)
class Fusion:
    def __init__(self, idx):
        self.id = idx
        self.i1 = FusionInterval(idx, 1)
        self.i2 = FusionInterval(idx, 2)
        self.numSpans = 0
        self.numPaired = 0
        self.genes1 = "none"
        self.genes2 = "none"
        self.transcripts1 = "none"
        self.transcripts2 = "none"
        self.biotypes1 = "none"
        self.biotypes2 = "none"
        self.expr1 = "none"
        self.expr2 = "none"
        self.filterStr = 0
        self.scores = {}
        self.struct_correct = {}
        self.bp_precision = {}
        self.breakpoints = []
        self.name = ""

class Aln:
    def __init__(self, chrName, startPos, strand, length ):
        self.startPos = startPos
        self.length = length
        self.chr = chrName
        self.strand = strand

    def __len__(self):
        return self.length

def getOverlappers( iv, geneModel):
    overlappers = set()

    for item in list(geneModel[iv].steps()):
        features = list(item[1])
        if len(features) > 0:
            for f in features:
                overlappers.add(f)

    return list(overlappers)


FUSION_SIZE = 50




def outputFusionFlankingSequences(fIv, overlappingExons, outstream, transcriptMap):

    outstream.write("Fusion part: %d-%d,  ref: %s, breakpoint:%d , direction: %s\n"
                    % (fIv.fusionId, fIv.id, fIv.ref, fIv.bpos, fIv.getDirection()))

    for feature in overlappingExons:
        fusionTranscriptId = feature.attr["transcript_id"]
        transcriptExons = transcriptMap[fusionTranscriptId]
        offset = feature.iv.start - fIv.bpos
        fusionExonNum = int(feature.attr["exon_number"])
        total_len = 0
        for exon in transcriptExons:
            exonNum = int(exon.attr["exon_number"])
            if exonNum < fusionExonNum:
                offset += exon.iv.length
            total_len += exon.iv.length

        #queryStart = 0
        #queryEnd = 0
        #if fIv.getDirection() == FUSION_DIRECTION_DOWNSTREAM:
        #    if feature.iv.getStrand() == '+':
        #        queryStart = offset
        #        queryEnd = offset + FUSION_SIZE
        #     else:
        #         queryStart = offset - FUSION_SIZE
        #         queryEnd = offset
        # else:
        #     if feature.iv.getStrand() == '+':
        #         queryStart = offset - FUSION_SIZE
        #         queryEnd = offset
        #     else:
        #         queryStart = offset
        #         queryEnd = offset + FUSION_SIZE

        outstream.write(" Overlapping transcript id: %s, strand: %s, offset in transcript: %d\n"
                        % (fusionTranscriptId, feature.iv.strand, offset))


def findOverlappingExons( fusion, geneModel ):
    (start1, end1) = (fusion.i1.startPos, fusion.i1.endPos)

    iv1 = HTSeq.GenomicInterval(fusion.i1.ref, start1, end1, ".")
    res1 = getOverlappers(iv1, geneModel)

    #(start2, end2) = ( min (fusion.startPos2, fusion.endPos2), max(fusion.startPos2, fusion.endPos2))
    (start2, end2) = ( fusion.i2.startPos, fusion.i2.endPos )

    iv2 = HTSeq.GenomicInterval(fusion.i2.ref, start2, end2, ".")
    res2 = getOverlappers(iv2, geneModel)

    return res1,res2


def getFeatureName(feature):
    return feature.attr["gene_name"] if "gene_name" in feature.attr else feature.name


def loadGeneModel(pathToGtf):
    #sys.stderr.write( "Loading gene models from %s\n" % pathToGtf  )

    model = HTSeq.GenomicArrayOfSets("auto", stranded=False)
    transcriptMap = defaultdict(list)

    gtfFile = HTSeq.GFF_Reader(pathToGtf)
    for feature in gtfFile:
        if feature.type == "exon":
            #feature_name = feature.attr["gene_name"] if "gene_name" in feature.attr else feature.name
            model[feature.iv] += feature
            transcriptMap[feature.attr["transcript_id"]].append(feature)

    #sys.stderr.write( "Finished loading gene models from %s\n" % pathToGtf )


    return model, transcriptMap


def parseRegion(regionStr):
    region = regionStr.split(",")
    assert len(region) == 3
    assert region[2] == FUSION_DIRECTION_DOWNSTREAM or region[2] == FUSION_DIRECTION_UPSTREAM


    return int(region[0]) + 1, int(region[1]) + 1, region[2]


def analyzeSeq(queryName, querySeq, seqIdList, outputPath, db_name, eValue ):
    (fd, queryFileName) = tempfile.mkstemp()
    tmp = os.fdopen(fd, "w")
    #print "Saving ", seq_id, seq_data
    tmp.write(">%s\n%s\n" % (queryName,querySeq))
    tmp.close()

    (fd,seqListFileName) = tempfile.mkstemp()
    seqIdListFile = os.fdopen(fd, "w")

    for line in seqIdList:
        seqIdListFile.write("%s\n" % line)
    seqIdListFile.close()

    cline = NcbiblastnCommandline(query=queryFileName, db=db_name,
#                                  task="megablast",
                                  task="blastn",
                                  evalue=eValue,
                                  seqidlist=seqListFileName, outfmt=5, out=outputPath)
    #print cline
    try:
        cline()
    except Exception as err:
        print err
        os.remove(outputPath)




def parseBlastResult(fileName):
    handle = open(fileName)
    blast_records = NCBIXML.parse(handle)

    results = []

    for record in blast_records:
        rec_id = str(record.query)

        if len(record.alignments) == 0:
            continue

        for algn in record.alignments:

            evalue = algn.hsps[0].expect

            score = 0
            ids = []

            for hsp in algn.hsps:
                score += hsp.bits
                ids.append(hsp.identities / float(hsp.align_length))

            max_identity = int(max(ids) * 100)
            seq_id = algn.hit_id

            results.append((rec_id, seq_id, evalue, max_identity, str(algn.hit_def).strip()))

    handle.close()

    return results


def writeHomologSearchQuery(fusion, exons, transcriptMap):
    outputPath = "/tmp/fusion_%d_homology_query" % fusion.id

    outFile = open(outputPath, "w")
    outFile.write("%s\n" % ",".join(getTranscriptNames(exons[0])))
    outFile.write("%s\n" % ",".join(getTranscriptNames(exons[1])))
    outputFusionFlankingSequences(fusion.i1, exons[0], outFile, transcriptMap)
    outputFusionFlankingSequences(fusion.i2, exons[1], outFile, transcriptMap)

    outFile.close()

def getBlastHits(queryName, querySeq, seqIdList, pathToBlastDb):

    outputPath = "/tmp/%s_blastOut.xml" % queryName


    analyzeSeq(queryName, querySeq, seqIdList, outputPath, pathToBlastDb, eValue=0.01)

    if os.path.exists(outputPath):
        return parseBlastResult(outputPath)
    else:
        print "Failed to run BLAST"
        return []


def searchForIntersection(features, alnResults):
    results = []

    for r in alnResults:
        if r[-1] in features:
            results.append(r)

    return results


def getTranscriptNames(exons):
    transcriptNames = set()

    for exon in exons:
        transcriptNames.add(exon.attr["transcript_id"].strip())

    return transcriptNames


def filterHomologPairs(fusion, exons, pathToBlastDB, transcriptMap):

    if len(pathToBlastDB) == 0:
        return False,False

    names1 = getTranscriptNames(exons[0])
    if len(names1) == 0:
        return False,False

    names2 = getTranscriptNames(exons[1])
    if len(names2) == 0:
        return False,False

    #writeHomologSearchQuery(fusion, exons, transcriptMap)

    alnResults1 = getBlastHits( "fusion_%d_1" % fusion.id , fusion.i1.seq, names2, pathToBlastDB)

    #set1 = searchForIntersection(getTranscriptNames(exons[1]), alnResults1)

    alnResults2 = getBlastHits( "fusion_%d_2" % fusion.id ,fusion.i2.seq, names1, pathToBlastDB)

    #set2 = searchForIntersection( getTranscriptNames(exons[0]), alnResults2)

    return  ( len(alnResults1) > 0, len(alnResults2) > 0 )


def getAttrValue(feature, name):
    if name in feature.attr:
        return feature.attr[name]
    else:
        return "no_attr"


def getGeneInfo(overlappedExons):

    geneNames = set()
    transcriptNames = set()
    biotypes = set()

    for item in overlappedExons:
        geneNames.add(getFeatureName(item))
        transcriptNames.add( "%s (exon %s)" % (getAttrValue(item, "transcript_name"),getAttrValue(item, "exon_number")) )
        biotypes.add( getAttrValue(item, "gene_biotype" ))

    return ";".join(geneNames),";".join(transcriptNames), ";".join(biotypes)

def getGeneInfos(transcripts):
    assert len(transcripts) == 2

    genes1, transcripts1, types1 = getGeneInfo(transcripts[0])

    genes2, transcripts2, types2 = getGeneInfo(transcripts[1])

    res1 = [genes1, genes2]
    res2 = [transcripts1, transcripts2]
    res3 = [types1, types2]

    return res1, res2, res3


def loadFusionsFromClusters(pathToFusionClusters, loadSupportingReads = False, idList = list(), combatiblityMode = False ):
    results = []
    f = None

    for line in open(pathToFusionClusters):
        if line.startswith("[START"):
            idx = int(line.split("CLUSTER")[1].split("]")[0])
            f = Fusion(idx)

        elif line.startswith("[END"):

            def swapIntervals(f):
                buf = f.i1
                f.i1 = f.i2
                f.i2 = buf

            if combatiblityMode:
                if f.i1.ref > f.i2.ref:
                    swapIntervals(f)
                elif f.i1.ref == f.i2.ref:
                    if f.i1.bpos > f.i2.bpos:
                        swapIntervals(f)

            append = True

            if idList:
                append = f.id in idList

            if append:
                results.append(f)

        elif line.startswith("sequence"):
            seqItems = line.split()
            f.i1.seq = seqItems[1]
            f.i2.seq = seqItems[2]
        elif line.startswith("annotation"):
            continue
        elif line.startswith("fusion"):
            items = line.split()
            assert len(items) >= 6
            f.i1.ref = items[1]
            f.i1.bpos = int(items[2]) + 1
            f.i1.startPos, f.i1.endPos, f.i1.dir = parseRegion(items[3])

            f.i2.ref = items[4]
            f.i2.bpos = int(items[5]) + 1
            f.i2.startPos, f.i2.endPos, f.i2.dir = parseRegion(items[6])

        else:
            #todo: infer this by using unique field
            items = line.split()
            flags = int(items[11])
            bp_type =  BP_BRIDGE_PAIR if flags & 0x4 else BP_SPAN

            if bp_type == BP_BRIDGE_PAIR:
                f.numPaired += 1
            else:
                f.numSpans += 1

            if loadSupportingReads:

                sepByIntron = flags & 0b11000000
                #TODO: add option to Aln object
                if sepByIntron:
                    continue

                aln1 =  Aln( items[1], int(items[2])+1, items[3], int(items[5]) )
                aln2 =  Aln( items[6], int(items[7])+1, items[8], int(items[10]) )

                f.breakpoints.append( [items[0], bp_type, aln1, aln2] )





    return results

def writeFilteredResults(fusions, outFile):

    sorted_fusions = sorted(fusions, key=lambda fusion: (fusion.numPaired + fusion.numSpans), reverse=True )

    outFile.write("#id\tref1\tbreak_pos1\tregion1\tref2\tbreak_pos2\tregion2\tnum_span\tnum_paired\tgenes_1\tgenes_2\tannotation\n")

    for f in sorted_fusions:
        outFile.write( "%s\t" % f.id )
        outFile.write( "%s\t%d\t[%d,%d]\t" % (f.i1.ref, f.i1.bpos, f.i1.startPos, f.i1.endPos) )
        outFile.write("%s\t%d\t[%d,%d]\t" % (f.i2.ref, f.i2.bpos, f.i2.startPos, f.i2.endPos) )
        outFile.write("%d\t%d\t" %  (f.numSpans, f.numPaired) )
        outFile.write("%s\t%s\t" % (f.genes1[0], f.genes2[0]) )
        outFile.write("%s\n" % ",".join( list(f.tools) ) )




def writeDetailedResults(results, outFile):

    #TODO: fix this

    sorted_results = sorted(results, key=lambda f: f.score, reverse=True)

    outFile.write("#id\tref1\tbreak_pos1\tregion1\tref2\tbreak_pos2\tregion2\tnum_span\tnum_paired\tgenes_1\tgenes_2\tfilters\n")

    #for f in sorted_results:
        #outFile.write( "%s\t" % f.id )
        #outFile.write( "%s\t%d\t[%d,%d]\t" % (f.i1.ref, f.i1.bpos, f.i1.startPos, f.i1.endPos) )
        #outFile.write("%s\t%d\t[%d,%d]\t" % (f.i2.ref, f.i2.bpos, f.i2.startPos, f.i2.endPos) )
        #outFile.write("%d\t%d\t" %  f.numSpans, f.numPaired)                      )
        #outFile.write("%s\t%s\t % )
        #fenes1, f.transcripts1, f.biotypes1, f.expr1,
        #f.genes2, f.transcripts2, f.biotypes2, f.expr2


def loadExpressionProfile(expressionPath):
    expr = {}

    for line in open(expressionPath):
        items = line.split()
        expr[items[0]] = int(items[1])

    return expr


def getExpressionValue(profile, genes):

    genesList = genes.split(";")

    vals = []
    for geneName in genesList:
        if geneName in profile:
            vals.append( str(profile[geneName]) )
        else:
            vals.append("0")

    return ";".join(vals)


def loadParologsMap(homologsPath):
    result = {}

    for line in open(homologsPath):
        items = line.split()
        geneName = items[0]
        if geneName not in result:
            result[geneName] = []

        homologName = items[2]
        if len(homologName) > 0:
            result[geneName].append(homologName)


    return result


def loadRepeats(repeatsPath):

    array = HTSeq.GenomicArrayOfSets("auto", stranded=False)

    sys.stderr.write("Loading repeat intervals from %s\n" % repeatsPath)

    for line in open(repeatsPath):

        if line.startswith("#"):
            continue

        items = line.split()


        chrName = items[5].replace("chr", "").replace("M","MT")
        startPos = int(items[6])
        endPos = int(items[7])

        #strand = items[9]

        iv = HTSeq.GenomicInterval(chrName, startPos, endPos)

        repeat = HTSeq.GenomicFeature(items[10], items[11] , iv )
        array[iv] += repeat


    sys.stderr.write( "Finished loading repeats from %s\n" % repeatsPath )

    return array


def filterRepeatRegions(fusion, repeatsIntervalArray):

    filters = []

    iv1 = HTSeq.GenomicInterval(fusion.i1.ref,  fusion.i1.startPos, fusion.i1.endPos, ".")
    res1 = getOverlappers(iv1, repeatsIntervalArray)

    if len(res1) > 0:
        filters.append("cluster_1_in_repeat_region")

    iv2 = HTSeq.GenomicInterval(fusion.i2.ref,  fusion.i2.startPos, fusion.i2.endPos, ".")
    res2 = getOverlappers(iv2, repeatsIntervalArray)

    if len(res2) > 0:
        filters.append("cluster_2_in_repeat_region")

    return filters



def reportResults(pathToFusionClusters, pathToGeneModels, pathToOutputFile, numSpans, numPairs, transcriptsDB="",
                  expressionPath="", parologsPath="", repeatsPath=""):


    results = loadFusionsFromClusters(pathToFusionClusters)

    geneModel = None
    transcriptMap = None
    expressionProfile = None
    repeatsIntervalArray = None

    if len(repeatsPath) > 0 and os.path.exists(repeatsPath):
        repeatsIntervalArray = loadRepeats(repeatsPath)

    if len(expressionPath) > 0 and os.path.exists(expressionPath):
        expressionProfile = loadExpressionProfile(expressionPath)

    parologsMap = None
    if os.path.exists( parologsPath ):
        parologsMap = loadParologsMap( parologsPath )

    for f in results:
        filters = []

        f.score = f.numSpans + f.numPaired

        if f.numSpans < numSpans or f.numPaired < numPairs:
            filters.append( "min_span")
            minSpanReached = False
        else:
            minSpanReached = True

        geneNames = ["", ""]
        transcriptNames = ["", ""]
        bioTypes = ["", ""]

        if len(pathToGeneModels) > 0:
            if geneModel is None:
                geneModel, transcriptMap = loadGeneModel(pathToGeneModels)
            overlappingExons = findOverlappingExons(f, geneModel)
            geneNames,transcriptNames,bioTypes = getGeneInfos(overlappingExons)
            if expressionProfile is not None:
                f.expr1 = getExpressionValue(expressionProfile,geneNames[0])
                f.expr2 = getExpressionValue(expressionProfile, geneNames[1])

            if parologsMap is not None:
                for geneName in geneNames[0]:
                    parologs = parologsMap[geneName]
                    for otherGeneName in geneNames[1]:
                        if otherGeneName in parologs:
                            filters.append("parologs")

            try:
                if minSpanReached:
                    res1,res2 = filterHomologPairs(f, overlappingExons, transcriptsDB, transcriptMap)
                    if res1 or res2:
                        filters.append( "homology:%s|%s" % (res1, res2) )
            except Exception as err:
                print "Failed to perform homology search for fusion ", f.id
                print "Reason: ", err

        if len(geneNames[0]) == 0:
            filters.append("cluster_1_out_of_gene")
        else:
            f.genes1 = geneNames[0]
            f.transcripts1 = transcriptNames[0]
            f.biotypes1 = bioTypes[0]

        if len(geneNames[1]) == 0:
            filters.append("cluster_2_out_of_gene")
        else:
            f.genes2 = geneNames[1]
            f.transcripts2 = transcriptNames[1]
            f.biotypes2 = bioTypes[1]

        if repeatsIntervalArray is not None:
            filters +=  filterRepeatRegions(f, repeatsIntervalArray)

        if "RNA" in f.biotypes1 or "RNA" in f.biotypes2:
            filters.append("non_coding_genome")

        f.filterStr = "pass" if len(filters) == 0 else ",".join(filters)


    filteredReport = open(pathToOutputFile, "w")
    writeFilteredResults(results, filteredReport)

    fullReport = open(pathToOutputFile + ".full", "w")
    writeDetailedResults(results, fullReport)


if __name__ == "__main__":

    descriptionText = "Print fusion report based on clusters output."

    parser = argparse.ArgumentParser(description=descriptionText, formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("clusters", help="Fusion clusters")
    parser.add_argument("-gtf", dest="gtfFile", default="", help="Input gtf file")
    parser.add_argument("-blastdb", dest="transcriptsDB", default="", help="Transcripts BLAST database")
    parser.add_argument("-out", dest="outFile", required="true", help="Output report file")
    parser.add_argument("-minSplit", dest="minSplit", type=int, default=2, help="Min split reads count")
    parser.add_argument("-minSpan", dest="minSpan", type=int, default=1, help="Min span pairs count")
    parser.add_argument("-pl", dest="parologsFile", default="", help="List of parologs")
    parser.add_argument("-rp", dest="repeatsFile", default="", help="Repeats annotation in UCSC track format")


    args = parser.parse_args()

    reportResults(args.clusters, args.gtfFile, args.outFile,args.minSplit, args.minSpan, args.transcriptsDB,
                  parologsPath = args.parologsFile, repeatsPath=args.repeatsFile)


