#include <seqan/bam_io.h>

#include "StringUtils.h"
#include "FindHomologsTask.h"

using namespace std;
using namespace seqan;


void FindHomologsTask::createGeneOverlapper()
{
    const TranscriptSet& transcripts = geneModels.getTranscripts();

    for (auto iter = transcripts.begin(); iter != transcripts.end(); ++iter) {
        const TranscriptPtr& t = iter->second;
        const vector<ExonPtr>& exons = t->getExons();
        // construct introns
        auto startIter = exons.begin();
        auto endIter = exons.end();

        for (auto iter = startIter; iter != endIter; ++iter) {
            const ExonPtr& exon = *iter;
            geneOverlapper.insertInterval(t->getRefName(), exon->startPos, exon->endPos(), exon);
         }
    }

}

bool FindHomologsTask::foundHomolog(Fusion &f, int intervalIndex, const string& alignmentHit)
{
    const FusionInterval& otherInterval = f.getEncompassingInterval(1 - intervalIndex);

    foreach_ (ExonPtr overlapper, otherInterval.overalppingExons) {

        TranscriptPtr transcript = overlapper->getTranscript();
        if (alignmentHit == transcript->getId()) {
            return true;
        }
    }

    return false;

}

bool FindHomologsTask::foundHomolog(Fusion &f, int intervalIndex, const string &refId, int pos)
{
    const FusionInterval& otherInterval = f.getEncompassingInterval(1 - intervalIndex);

    if (refId == otherInterval.refId) {

        if (otherInterval.direction == Cluster::DirectionLeft) {
            if (pos >= otherInterval.end && pos <= (otherInterval.end + cfg.meanIntronLength) ) {
                return true;
            }
        } else {
            if (pos >= (otherInterval.begin - cfg.meanIntronLength) && pos <= otherInterval.begin) {
                return true;
            }
        }
    }

    return false;

}


bool FindHomologsTask::analyzeTranscriptomicAlignments()
{
    BamAlignmentRecord record;
    BamStream bamStreamIn(cfg.transcriptomicAlignmentPath.c_str(), BamStream::READ);

    if (!isGood(bamStreamIn)) {
        cerr << "Can not open input BAM file: " << cfg.transcriptomicAlignmentPath << endl;
        return false;
    }

    while (!atEnd(bamStreamIn))
    {

        if (readRecord(record, bamStreamIn) != 0)
        {
            std::cerr << "Could not read alignment!" << std::endl;
            return 1;
        }

        if (hasFlagUnmapped(record)) {
            continue;
        }

        string ref(toCString( bamStreamIn._nameStore[record.rID] ));
        string idStr(toCString(record.qName) );

        vector<string> idItems = StringUtils::split(idStr, '/');
        if (idItems.size() == 2) {
            bool ok = false;
            int fusionIdx = StringUtils::parseNumber<int>(idItems[0], ok);
            assert(ok);
            int intervalIdx = StringUtils::parseNumber<int>(idItems[1], ok) - 1;
            assert(ok);
            assert(intervalIdx == 0 || intervalIdx == 1);
            if (ok)  {
                assert(fusionIdxMap.count(fusionIdx) > 0);
                if (fusionIdxMap.count(fusionIdx) > 0) {
                    Fusion& f = fusions[ fusionIdxMap.at(fusionIdx) ];
                    if (foundHomolog(f, intervalIdx, ref)) {
                        stringstream ss;
                        ss << intervalIdx;
                        f.setAnnotationValue( ANNOTATION_HOMOLOGOUS_GENES, ss.str() );
                    }
                } else {
                    cerr << "WARNING: fusion " << fusionIdx << " is not found." << endl;
                }

            }

        }
    }


    return true;

}


bool FindHomologsTask::analyzeGenomicAlignments()
{
    BamAlignmentRecord record;
    BamStream bamStreamIn(cfg.genomicAlignmentPath.c_str(), BamStream::READ);

    if (!isGood(bamStreamIn)) {
        cerr << "Can not open input BAM file: " << cfg.genomicAlignmentPath << endl;
        return false;
    }

    while (!atEnd(bamStreamIn))
    {

        if (readRecord(record, bamStreamIn) != 0)
        {
            std::cerr << "Could not read alignment!" << std::endl;
            return 1;
        }

        if (hasFlagUnmapped(record)) {
            continue;
        }

        string ref(toCString( bamStreamIn._nameStore[record.rID] ));
        string idStr(toCString(record.qName) );

        vector<string> idItems = StringUtils::split(idStr, '/');
        if (idItems.size() != 2) {
            continue;
        }
        bool ok = false;
        int fusionIdx = StringUtils::parseNumber<int>(idItems[0], ok);
        assert(ok);
        int intervalIdx = StringUtils::parseNumber<int>(idItems[1], ok) - 1;
        assert(ok);
        assert(intervalIdx == 0 || intervalIdx == 1);

        if (ok)  {
                assert(fusionIdxMap.count(fusionIdx) > 0);
                if (fusionIdxMap.count(fusionIdx) > 0) {
                    Fusion& f = fusions[ fusionIdxMap.at(fusionIdx) ];

                    if (foundHomolog(f,intervalIdx, ref, record.beginPos))  {
                        f.setAnnotationValue(ANNOTATION_LOCAL_HOMOLOGY, "true");
                    }

                }
        }
    }


    return true;
}

void FindHomologsTask::overlapFusionIntervalWithGenes(Fusion& fusion, int idx)
{

    FusionInterval& iv = fusion.getEncompassingInterval(idx);
    std::vector<ExonPtr> results;
    geneOverlapper.findIntervals(results, iv.refId, iv.begin, iv.end );


    foreach_ (ExonPtr exon, results) {
        iv.overalppingExons.insert(exon);
    }

}


int FindHomologsTask::run()
{

    if (!Fusion::readFusionClusters(fusions, cfg.inputFusionsPath)) {
        return -1;
    }

    for (size_t i = 0; i < fusions.size(); ++i ) {
        int fusionIndex = fusions.at(i).getIndex();
        fusionIdxMap[fusionIndex] = i;
    }


    if (cfg.gtfPath.size() > 0 && cfg.transcriptomicAlignmentPath.size() > 0) {

        cerr << "Loading transcript annotations..." << endl;

        if (! geneModels.loadTranscripts(cfg.gtfPath)) {
            cerr << "Failed to load gene models from " << cfg.gtfPath << endl;
            return -1;
        }
        cerr << "Creating gene overlapper" << endl;

        createGeneOverlapper();

        foreach_ (Fusion& f, fusions) {
            overlapFusionIntervalWithGenes(f, 0);
            overlapFusionIntervalWithGenes(f, 1);
        }

        cerr << "Processing transcriptomic alignments" << endl;

        if (!analyzeTranscriptomicAlignments()) {
            return -1;
        }

    }

    if (cfg.genomicAlignmentPath.size() > 0) {
        cerr << "Processing genomic cluster alignments" << endl;

        if (!analyzeGenomicAlignments()) {
            return false;
        }
    }


    if (!Fusion::writeFusionClusters(fusions, cfg.outputFusionsPath)) {
        return -1;
    }


    return 0;
}
