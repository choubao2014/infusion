#ifndef GENOMIC_INTERVAL_H
#define GENOMIC_INTERVAL_H

#include <ostream>
#include "Global.h"

class GenomicInterval
{
public:

    int begin, end;
    string refId;
    bool isPositiveStrand;

    GenomicInterval() {
        begin = -1;
        end = -1;
    }

    GenomicInterval(const string& refIdx, int startPos, int endPos) {
        refId = refIdx;
        begin = startPos;
        end = endPos;
    }

    bool operator<(const GenomicInterval& other) const {

        if (refId == other.refId) {
            return begin < other.begin;
        } else {
            return refId < other.refId;
        }
    }

    inline int length() const { return end - begin + 1; }

    inline bool intersects(const GenomicInterval& r) const {
        if (refId != r.refId) {
            return false;
        }
        int sd = begin - r.begin;
        return (sd >= 0) ? (sd < r.length()) : (-sd < length());
    }

    char strandToChar() const { return isPositiveStrand ? '+' : '-'; }

    bool isRevCompl() const { return !isPositiveStrand; }

    void joinInterval(const GenomicInterval& r);
    void joinInterval(int start, int end);

    static bool contains(int startPos, int length, int pos) {
        return (pos >= startPos && pos < startPos + length);
    }

    static void print(std::ostream& os, const GenomicInterval& iv) {
        os << "[" << iv.refId << "," << iv.begin << "," << iv .end
                  << "," << iv.strandToChar() << "]";
    }


};




#endif // GENOMIC_INTERVAL_H
