#ifndef CLUSTER_H
#define CLUSTER_H


#include <boost/shared_ptr.hpp>
#include <seqan/sequence.h>

#include "BreakpointCandidate.h"
#include "SimpleInterval.h"

class Cluster;
typedef boost::shared_ptr<Cluster> ClusterPtr;

class ClusterSegment;
typedef boost::shared_ptr<ClusterSegment> ClusterSegmentPtr;

class ClusterSegment {
    ClusterPtr parent;
    // TODO: this should be a node of a graph, since there could be more than 1 breakpoint
    ClusterSegmentPtr sibling;
    BreakpointCandidatePtr bp;
    const int localIdx;
    bool processed, first;
public:
    ClusterSegment(const ClusterPtr& p, const BreakpointCandidatePtr& ptr, int idx) :
        parent(p), bp(ptr), localIdx(idx), processed(false), first(false) { assert(p); }
    bool isProcessed() { return processed; }
    void setProcessed() { processed = true; }
    const ClusterPtr& getParentCluster() const { return parent; }
    void setParentCluster(const ClusterPtr& newParent ) { parent = newParent; }
    const ClusterSegmentPtr& getPartner() const { return sibling; }
    void setPartner(const ClusterSegmentPtr& segment) { sibling = segment; }
    const BreakpointCandidatePtr& getBreakpointCandidate() const { return bp; }
    void markSeparatedByIntron() { bp->setSeparatedByIntron(localIdx); }
    bool isSeparatedByIntron() {
        if (localIdx == 0) {
            return bp->isFirstSeparatedByIntron();
        } else {
            return bp->isSecondSeparatedByIntron();
        }
    }
    int getLocalIdx() const { return localIdx; }
    void setFirst() { first = true; }
    bool isFirst() const { return first; }
    const QueryAlignment& getAlignment() const { return bp->alns.at(localIdx); }
};

class Cluster {
public:
    enum Direction {
        DirectionRight, DirectionLeft, DirectionUnknown
    };
    struct MaxNumberOfBreakpoints {
        bool operator()(const ClusterPtr& l, const ClusterPtr& r) const {
            return l->getSegments().size() > r->getSegments().size();
        }
    };
    //set<ClusterPtr,Cluster::MaxNumberOfBreakpoints>
    typedef std::set<ClusterPtr> PtrSet;
    static int insertSizeTolerance, maxFuzzyBreakpointDistance;
private:
    int idx, sequenceOffset;
    string refId;
    mutable SimpleInterval interval;
    mutable int bPos, numLocalAlignments;
    mutable bool uptodate;
    mutable seqan::CharString clusterSubseq;
    std::vector<ClusterSegmentPtr> segments;
    seqan::CharString sequenceData;
    Direction dir;
    mutable std::set<ClusterPtr> uniquePartners;
    void updateSequence(int intervalStart, int intervalLen) const;
public:
    Cluster(int id, const string& ref, Direction d) : idx(id), sequenceOffset(0), refId(ref), interval(-1,0), bPos(-1),
        uptodate(false), dir(d) {}

    int getIdx() const { return idx; }

    void update() const;

    const SimpleInterval& getEncompassingInterval() const {
        if ( !uptodate ) {
            update();
        }

        return interval;
    }

    void separateByBreakpoint(vector<ClusterPtr>& results, int& idx);

    int getBreakpointPos() const {
        if ( !uptodate ) {
            update();
        }
        return bPos;
    }

    bool isUptodate() const {
        return uptodate;
    }

    static void merge(ClusterPtr& dest, ClusterPtr& other);

    const seqan::CharString& getSequence() const {
        if (!uptodate) {
            update();
        }

        return clusterSubseq;
    }

    bool canAddSegment(const ClusterSegmentPtr& segment);

    void setSequence(const CharString& seq, int intervalStart, int intervalLen, int startOffset);

    void addSegment(const ClusterSegmentPtr& segment) {
        assert(canAddSegment(segment));
        segments.push_back(segment);
        uptodate = false;
    }

    void insertSegments(const std::vector<ClusterSegmentPtr>& toAdd ) {
        // TODO: why in the beginning?
#ifdef DEBUG
        foreach_ (ClusterSegmentPtr& s, segments) {
            assert(canAddSegment(s));
        }
#endif
        segments.insert(segments.begin(), toAdd.begin(), toAdd.end());
        uptodate = false;
    }

    void clear() {
        segments.clear();
        seqan::clear(sequenceData);
        seqan::clear(clusterSubseq);
        foreach_( ClusterPtr partner, uniquePartners ) {
            partner->uptodate = false;
        }

        uptodate = false;
    }

    void removeSegment(const ClusterSegmentPtr& segment) {
        std::vector<ClusterSegmentPtr>::iterator pos = std::find(segments.begin(), segments.end(), segment);
        if (pos != segments.end()) {
            segments.erase(pos);
            uptodate = false;
        }

    }

    const std::vector<ClusterSegmentPtr>& getSegments() const {
        return segments;
    }

    const size_t getNumSegments() const {
        return segments.size();
    }

    const string& getRefId() const {
        return refId;
    }

    const Direction getDirection() const {
        return dir;
    }

    int getNumLocalAlignments() const {
        return numLocalAlignments;
    }

    /*std::set<ClusterPtr,MaxNumberOfBreakpoints> getUniquePartners() const {
        std::set<ClusterPtr,MaxNumberOfBreakpoints> partnerClusters;
        foreach_(const ClusterSegmentPtr& segment, segments) {
            partnerClusters.insert(segment->getPartner()->getParentCluster());
        }

        return partnerClusters;
    }*/

    void updateUniquePartners();

    const std::set<ClusterPtr>& getUniquePartners() const {
        return uniquePartners;
    }


    static bool  alignmentConcordantWithCluster(const QueryAlignment& aln, bool firstAlignment, const Cluster* cluster) ;
    static Direction inferDirection(bool alnIsPositiveStrand, bool firstAlignment);

    static bool alnShouldHavePositiveStrandInCluster(Cluster::Direction dir, bool firstAlignment) {
        if (firstAlignment) {
            return dir == DirectionLeft;
        } else {
            return dir == DirectionRight;
        }
    }


    static char directionToChar(Direction d) {
        if (d == DirectionLeft) {
            return '<';
        } else if (d == DirectionRight) {
            return '>';
        } else {
            assert(0);
            return '=';
        }
    }

    static Direction directionFromChar(char dc) {
        if (dc == '<') {
            return DirectionLeft;
        } else if (dc == '>') {
            return DirectionRight;
        } else {
            assert(0);
            return DirectionUnknown;
        }
    }

};

inline std::ostream& operator<<(std::ostream& os, const ClusterPtr& cl)
{
   os << "[" << "idx=" << cl->getIdx() << ",";
   os << "start=" << cl->getEncompassingInterval().begin << ",";
   os << "end=" << cl->getEncompassingInterval().end << ",";
   os << "n=" << cl->getNumSegments() << ",";
   os <<  "]";
   return os;
}


inline std::ostream& operator<<(std::ostream& os, const ClusterSegmentPtr& s)
{
   os << "[" << "localIdx=" << s->getLocalIdx() << ",";
   os << "name=" << s->getBreakpointCandidate()->readName << ",";
   const QueryAlignment& aln = s->getAlignment();
   os << "ref=" << aln.refId << ",";
   os << "start=" << aln.refStart << ",";
   os << "end=" << aln.inRefEnd() << ",";
   if (s->getBreakpointCandidate()->isOriginLocal()) {
       os << "local";
   } else if (s->getBreakpointCandidate()->isOriginPaired()) {
       os << "paired";
   }
   os <<  "]";
   return os;
}


inline void debugOutput(const Cluster* cluster) {
    using namespace std;
    int local = 0, rescued = 0;
    auto segments = cluster->getSegments();
    foreach_ (ClusterSegmentPtr s, segments) {
        cerr << s->getBreakpointCandidate()->readName << ",";
        cerr << s->getAlignment().refStart << "-" << s->getAlignment().inRefEnd() << endl;
        if (s->getBreakpointCandidate()->isOriginLocal()) {
            local++;
            if (s->getBreakpointCandidate()->isRedeemed()) {
                rescued++;
            }
        }
    }
    cerr << "Local=" << local << ",rescued=" << rescued << endl;
}

#endif // CLUSTER_H
