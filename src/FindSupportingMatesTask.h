#ifndef FIND_SUPPORTING_MATES_TASK_H
#define FIND_SUPPORTING_MATES_TASK_H

#include <string>
#include <map>
#include <unordered_map>
#include <boost/scoped_ptr.hpp>

#include "BreakpointCandidate.h"
#include "AnnotationModel.h"
#include "ChunkBamReader.h"


using std::string;
using std::unordered_map;

typedef std::unordered_map< string,vector<BreakpointCandidatePtr> > BreakpointCandidateMap;

struct FindSupportingMatesConfig {
    string pathToGtf;
    string pathToGenomicAlns1, pathToGenomicAlns2;
    string pathToTranscriptomeAlns1, pathToTranscriptomeAlns2;
    string pathToFusions;
    string pathToOutputFusions;
    int maxMateDistance;
    FindSupportingMatesConfig() : maxMateDistance(20000) {}
};



class ReadMateAlignmentChecker {
public:
    virtual bool genomicReadHasMateInTranscript(const TranscriptPtr& transcript, const QueryAlignment& aln, bool isUpstreamMate, const seqan::BamAlignmentRecord& possibleMate) = 0;
};

class SimpleMateChecker : public ReadMateAlignmentChecker {
public:
    virtual bool genomicReadHasMateInTranscript(const TranscriptPtr& transcript, const QueryAlignment& aln, bool isUpstreamMate, const seqan::BamAlignmentRecord& possibleMate) {
        return ( (transcript->getRefName() == aln.refId)  && ( transcript->geneContainsGenomicPoint(aln.refStart) ) );
    }
};


class GeneBoundaryMateChecker : public ReadMateAlignmentChecker {
public:
    virtual bool genomicReadHasMateInTranscript(const TranscriptPtr &transcript, const QueryAlignment &aln, bool isUpstreamMate, const seqan::BamAlignmentRecord &possibleMate);
};


class InsertSizeBasedMateChecker : public ReadMateAlignmentChecker {
private:
    int maxMateDistance;
public:
    InsertSizeBasedMateChecker(int maxInsertSize) : maxMateDistance(maxInsertSize) {

    }
    virtual bool genomicReadHasMateInTranscript(const TranscriptPtr &transcript, const QueryAlignment &aln, bool isUpstreamMate, const seqan::BamAlignmentRecord &possibleMate);
};


class FindSupportingMatesTask
{
    FindSupportingMatesConfig cfg;
    AnnotationModel geneModel;
    boost::scoped_ptr<ReadMateAlignmentChecker> mateConditionChecker;

public:
    FindSupportingMatesTask(FindSupportingMatesConfig& config)
        : cfg(config), mateConditionChecker(new InsertSizeBasedMateChecker(config.maxMateDistance)) {

    }

    static bool checkSecondMateInTranscript(const TranscriptPtr& transcript, const QueryAlignment& aln, bool isUpstreamMate, const seqan::BamAlignmentRecord& possibleMate);

    bool findMatesFromAlignments( BreakpointCandidateMap& queryBreakpoints, bool upstreamMates, const string& pathToTranscriptAlignments, bool transcriptomic);

    int performAnalysis();
};

#endif // FIND_SUPPORTING_MATES_TASK_H
