#ifndef CLUSTER_TREE_H
#define CLUSTER_TREE_H

#include <stddef.h>
#include <vector>

#include <boost/shared_ptr.hpp>

extern "C" {
    #include "3rd_party/cluster_tree/cluster.h"
}

using std::vector;

/**
 * Wrapper around cluster tree from bx-python library
 * https://bitbucket.org/james_taylor/bx-python
 */


template<typename TValue>
class ClusterTree
{
    clustertree* tree;
    vector<TValue> items;
    int curIdx;

public:
    ClusterTree(int maxClusterDistance = 0)  {
        curIdx = 0;
        tree = create_clustertree(maxClusterDistance, 1);

    }

    ~ClusterTree() {
        free_tree(tree);
        tree = NULL;
    }

    class Node {
        ClusterTree& clusterTree;
        treeitr* iter, *rootIter;
    public:

        Node(ClusterTree& tree) : clusterTree(tree)  {
            rootIter = clusteritr(clusterTree.tree);
            iter = rootIter;
        }

        ~Node() {
            freeclusteritr(rootIter);
        }

        clusternode* getClusterNode() {
            return iter->node;
        }

        void getIntervalItems(vector<TValue>& results) const {

            clusternode* node = iter->node;
            struct_interval* interval = node->interval_head;
            while ( interval != NULL) {
                const TValue& item = clusterTree.items.at( interval->id );
                results.push_back( item );
                interval = interval->next;
            }

        }

        bool isNull() { return iter == NULL; }

        void stepForward() {
            if (iter != NULL ) {
                iter = iter->next;
            }
        }

    };

    clusternode* insertInterval(int begin, int end, const TValue& val) {
        int curIdx = items.size();
        tree->root = clusternode_insert(tree, tree->root, begin, end, curIdx);
        items.push_back(val);

        return tree->modified_node;
    }

    Node leftMostCluster()  {
        return Node(*this);
    }

};


#endif // CLUSTER_TREE_H
