#include <iostream>
#include <vector>

#include <seqan/arg_parse.h>
#include "AnnotateFusionsTask.h"
#include "FindHomologsTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "remove_homologs"

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);

    addDescription(parser, "This tool is part of the InFusion pipeline. It takes fusion clusters as input and annotates homologous fusions based on the given fusion sequence alignment");

    string usageLine;
    usageLine.append(" [OPTIONS] -i fusionClusters.txt -o updatedClusters.txt");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");
    addOption(parser, ArgParseOption( "i", "input", "Path to input fusion clusters. Use - to for standard input.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "x", "--intron-size", "Mean intron length. Default is 10000.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "o", "output", "Path to output updated fusion clusters. Use - to for standard output", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "gtf", "annotation", "Path to annotations in GTF format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "b", "transcriptome-alignment", "Path to fusion seq to transcriptome alignment in SAM format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "l", "clusters-alignment", "Path to fusion seq alignment to genomic regions upstream or downstream of breakpoint in SAM format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "g", "genome-alignment", "Path to fusion seq alignment to genome in SAM format", ArgParseArgument::STRING ));


    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}


int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    FindHomologsTaskConfig cfg;

    getOptionValue(cfg.inputFusionsPath, parser, "i");
    getOptionValue(cfg.outputFusionsPath, parser, "o");
    getOptionValue(cfg.transcriptomicAlignmentPath, parser, "b");
    getOptionValue(cfg.meanIntronLength, parser, "x");
    getOptionValue(cfg.genomicAlignmentPath, parser, "g");
    getOptionValue(cfg.clusterAlignmentPath, parser, "l");
    getOptionValue(cfg.gtfPath, parser, "gtf");

    FindHomologsTask task(cfg);

    return task.run();

}





