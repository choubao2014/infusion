#ifndef FEATURE_H
#define FEATURE_H

#include <boost/shared_ptr.hpp>
#include <memory>
#include "Global.h"


struct FeatureData
{
    string id, seqName;
    int startPos;
    int length;
    bool positiveStranded;
    int endPos() const { return startPos + length - 1 ; }
    FeatureData() : startPos (-1), length(0) {}
    FeatureData(const string& sName) : id(sName), startPos(-1), length(0) {}
    FeatureData(const string& fName, const string& sName, int p, int l, bool isPositiveStrand )
        : id(fName), seqName(sName), startPos(p),
          length(l), positiveStranded(isPositiveStrand) {}
    bool operator<(const FeatureData& other) const {

        if (seqName == other.seqName) {
            return startPos < other.startPos;
        } else {
            // lexigraphical comparison
            return seqName < other.seqName;
        }
    }
    bool contains(int pos) {
        return ( pos >= startPos && pos <= endPos());
    }
};

typedef boost::shared_ptr<FeatureData> FeaturePtr;
typedef map<string,FeaturePtr> FeatureSet;

struct FeatureCompare {
    bool operator()(const FeaturePtr& l, const FeaturePtr& r) {
        return *l < *r;
    }
};


#endif // FEATURE_H
