#ifndef PERF_TIMER_H
#define PERF_TIMER_H

#include <ctime>



class PerfTimer
{
    clock_t t1;
    double elapsed;
public:
    PerfTimer() : t1(clock()), elapsed(0) {

    }

    void reset() {
        t1 = clock();
    }

    void drop() {
        elapsed += ( std::clock() - t1 ) / (double) CLOCKS_PER_SEC;
    }

    double getElapsed() { return elapsed; }

};

#include <time.h>

// TODO: reuse boost::timer::cpu_timer available from 1.47?

class CpuPerfTimer
{
    struct timespec start, finish;
    double elapsed;
public:
    CpuPerfTimer() : elapsed(0) {
        clock_gettime(CLOCK_MONOTONIC, &start);
    }

    void reset() {
        clock_gettime(CLOCK_MONOTONIC, &start);
    }

    void drop() {
        clock_gettime(CLOCK_MONOTONIC, &finish);
        elapsed += (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    }

    double getElapsed() { return elapsed; }

};


#endif // PERF_TIMER_H
