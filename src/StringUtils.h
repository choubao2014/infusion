#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <vector>
#include <string>
#include <sstream>
#include <boost/lexical_cast.hpp>
#include <boost/unordered_map.hpp>

#include <seqan/sequence.h>


using std::vector;
using std::string;
using std::stringstream;
using seqan::CharString;
using seqan::StringSet;

struct SearchResult {
    SearchResult() : pos(-1), isPositiveStrand(true), score(0) {}
    SearchResult(int p, bool isPositive, int s = 0) : pos(p), isPositiveStrand(isPositive), score(s) {}
    int pos;
    bool isPositiveStrand;
    int score;
};


struct CompareSearchResults {
    bool operator()(const SearchResult& l, const SearchResult& r) {
        return l.score > r.score;
    }

};

/* Well, this can be easily replaced with BOOST stuff
 * Maybe I should use it ... */

class StringUtils
{
public:
    static vector<string> split(const string& str, char separator);

    static string substrAfterPattern(const string& str, const string& pattern);

    static string substrBeforePattern(const string& str, const string& pattern);

    template <typename T>
    static T parseNumber(const string& line, bool& ok) {
        stringstream ss(line);
        T res = -1;
        ss >> res;
        ok = !ss.fail();

        return res;
    }

    template <typename container_t>
    static string join(const container_t& stringList, const string& sep) {
        string res;
        int idx = 0;

        for (auto it = stringList.begin(); it != stringList.end(); ++it ) {

            const string& str = (*it);
            if (idx > 0) {
                res += sep;
            }
            res += str;
            idx++;
        }

        return res;
    }


    template <typename num_t>
    static string numToStr(const num_t& num) {
        return boost::lexical_cast<string>(num);
    }

    static bool startsWith(const string& line, const string& prefix);

    static string trim(const string& line);

    static vector<string> splitFileName(const string& line);

    static size_t findFlankingPattern(const string& pattern, const string& source, bool searchDownstream, int tolerance);

    // searches in both strands
    static vector<SearchResult> findDnaPattern(const CharString& query, const CharString& source, int minScore);

    // reverse-complements the pattern
    static vector<SearchResult> findDnaPattern(CharString& query, CharString& source, int minScore, bool revCompl);


    static string removeReadMateIdentifer(const CharString &readName);

    static void removeReadMateIdentiferInPlace(string& readName);

    static void addReadMateIdentifier(CharString& readName, char id);

    static void convertStringSetToVector(const StringSet<CharString>& stringSet, vector<string>& array);



};


#endif // STRING_UTILS_H
