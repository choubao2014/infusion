#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>

#include "StringUtils.h"
#include "GlobalContext.h"

#include "AnnotateFusionsTask.h"

#include <unordered_map>

using namespace std;


HomologyAnnotator::HomologyAnnotator(const string &pathToTranscripts)
{
    // load transcripts

    transcripts.reset( new ReferenceSeqInMemory() );

    bool ok = transcripts->load( pathToTranscripts );
    if (!ok) {
        cerr << "Failed to load transcript sequences from " << pathToTranscripts;
    }


}

AlignmentResult HomologyAnnotator::testFusionIntervalForHomology(const Fusion &f, int intervalIndex)
{
    AlignmentResult finalResult;

    const CharString& targetSeq = f.getSequence(intervalIndex);

    const FusionInterval& otherInterval = f.getEncompassingInterval(1 - intervalIndex);

    foreach_ (ExonPtr overlapper, otherInterval.overalppingExons) {

        TranscriptPtr transcript = overlapper->getTranscript();
        const vector<ExonPtr>& transcriptExons = transcript->getExons();
        int offset = otherInterval.breakpointPos - overlapper->startPos;
        int offsetInTranscript = 0;
        foreach_ (ExonPtr exon, transcriptExons) {
            if (exon == overlapper) {
                break;
            } else {
                offsetInTranscript += exon->length;
            }
        }

        offsetInTranscript += offset;
        int hostStart = 0;
        int hostLength = 0;
        int transcriptLength = transcript->getLength();

        if (otherInterval.direction == Cluster::DirectionLeft) {
            hostStart = offsetInTranscript;
            hostLength = transcriptLength - offsetInTranscript - 1;
        } else {
            hostStart = 0;
            hostLength = offsetInTranscript;
        }

        int targetLength = length(targetSeq);
        if (hostLength < targetLength ) {
            continue;
        }

        if (!transcript->isPositiveStranded()) {
            hostStart = hostStart == 0 ? transcriptLength - hostLength : 0;
        }

        if (hostStart < 0) {
            hostStart = 0;
        }
        if (hostStart + hostLength >= transcriptLength) {
            hostLength = transcriptLength - hostStart - 1;
        }

        //cerr << transcript->getName() << " " << queryStart << " " << queryEnd << endl;
        CharString otherSeq = transcripts->getSubSequence(transcript->getId(), hostStart, hostLength);
        if (length(otherSeq) == 0 || length(targetSeq) == 0) {
            continue;
        }

        //cerr << "Query  : " << targetSeq << endl;
        //cerr << "Host   : " << otherSeq << endl;

        AlignmentResult res = AlignmentUtils::performLocalAlignment(otherSeq, targetSeq );
        if (res.score > finalResult.score) {
            finalResult = res;
        }

        reverseComplement(otherSeq);
        //cerr << "Host-rc: " << otherSeq << endl;

        AlignmentResult res2 = AlignmentUtils::performLocalAlignment(otherSeq, targetSeq);
        if (res2.score > finalResult.score) {
            finalResult = res2;
        }

        if (finalResult.percentageIdentity >= 95 ) {
            break;
        }


    }

    return finalResult;

}

string alignmenResultToStr(const AlignmentResult& r) {
    return StringUtils::numToStr(r.score) + "," + StringUtils::numToStr(r.percentageIdentity);
}


void HomologyAnnotator::annotate(Fusion& fusion)
{
    AlignmentResult r1 = testFusionIntervalForHomology(fusion, 0);
    fusion.setAnnotationValue(ANNOTATION_HOMOLOGY_1, alignmenResultToStr(r1) );

    AlignmentResult r2 = testFusionIntervalForHomology(fusion, 1);
    fusion.setAnnotationValue(ANNOTATION_HOMOLOGY_2, alignmenResultToStr(r2) );

}

///////////////////////////////////////////////////////////////////////////////////////////////////

HomologousGenesAnnotator::HomologousGenesAnnotator(const string& pathToBamFile)  {
    loadAlignmentsOk = loadAlignments(pathToBamFile);
}

bool HomologousGenesAnnotator::loadAlignments(const string &inFile)
{
    using namespace seqan;

    BamAlignmentRecord record;
    BamStream bamStreamIn(inFile.c_str(), BamStream::READ);

    if (!isGood(bamStreamIn)) {
        cerr << "Can not open input BAM file: " << inFile << endl;
        return false;
    }

    while (!atEnd(bamStreamIn))
    {

        if (readRecord(record, bamStreamIn) != 0)
        {
            std::cerr << "Could not read alignment!" << std::endl;
            return 1;
        }

        if (hasFlagUnmapped(record)) {
            continue;
        }

        string ref(toCString( bamStreamIn._nameStore[record.rID] ));
        string idStr(toCString(record.qName) );

        vector<string> idItems = StringUtils::split(idStr, '/');
        if (idItems.size() == 2) {
            bool ok = false;
            int fusionIdx = StringUtils::parseNumber<int>(idItems[0], ok);
            assert(ok);
            int intervalIdx = StringUtils::parseNumber<int>(idItems[1], ok);
            assert(ok);
            if (ok)  {
                FusionIntervalIdx idx(fusionIdx, intervalIdx);
                alignmentMap[idx].insert (ref);
            }

        }

    }

    return true;

}


bool HomologousGenesAnnotator::hasHomologousAlignments(const Fusion &f, int intervalIndex)
{
    FusionIntervalIdx idx(f.getIndex(), intervalIndex + 1);
    auto iter = alignmentMap.find(idx);
    if (iter != alignmentMap.end()) {

        const set<string>& alignmentHits = iter->second;
        const FusionInterval& otherInterval = f.getEncompassingInterval(1 - intervalIndex);

        foreach_ (ExonPtr overlapper, otherInterval.overalppingExons) {

            TranscriptPtr transcript = overlapper->getTranscript();
            if ( alignmentHits.count( transcript->getId() ) != 0 ) {
                return true;
            }
        }
    }

    return false;
}



void HomologousGenesAnnotator::annotate(Fusion &fusion) {
    if (!loadAlignmentsOk) {
        return;
    }

    bool res1 = hasHomologousAlignments(fusion, 0);
    bool res2 = hasHomologousAlignments(fusion, 1);

    if (res1 || res2) {
        stringstream ss;
        ss << res1 << "|" << res2;
        fusion.setAnnotationValue(ANNOTATION_HOMOLOGOUS_GENES, ss.str());
    }


}


///////////////////////////////////////////////////////////////////////////////////////////////////

// TODO: this function is redundant! remove it!

static inline const int getAlignmentIdx(const BreakpointCandidatePtr bp, const FusionInterval& iv) {
    const QueryAlignment& aln0 = bp->alns.at(0);
    GenomicInterval alnIv(aln0.refId, aln0.refStart, aln0.inRefEnd());
    if (iv.intersects(alnIv)) {
        return 0;
    } else {
        return 1;
    }
}

float StrandnessAnnotator::annotateStrandness(const Fusion &f, int idx)
{
    float res = -1;

    int numConcordant = 0;

    const FusionInterval& iv = f.getEncompassingInterval(idx);

    if (iv.overalppingExons.size() > 0) {

        TranscriptPtr t = (*iv.overalppingExons.begin())->getTranscript();
        auto bpList = f.getBreakpoints();

        foreach_ (BreakpointCandidatePtr bp, bpList ) {
            size_t alnIdx = getAlignmentIdx(bp, iv);
            if (bp->alns[alnIdx].positiveStrand == t->isPositiveStranded()) {
                numConcordant += 1;
            }
        }

        res = float(numConcordant) / bpList.size();
    }


    return res;
}

void StrandnessAnnotator::annotate(Fusion &f)
{
    if (f.getBreakpoints().size() == 0) {
        return;
    }

    float res1 = annotateStrandness(f, 0);
    float res2 = annotateStrandness(f, 1);

    stringstream ss;

    if (res1 < 0) {
        ss << ".";
    } else {
        ss << res1;
    }

    ss << ";";

    if (res2 < 0) {
        ss << ".";
    } else {
        ss << res2;
    }


    f.setAnnotationValue(ANNOTATION_STRANDNESS, ss.str());


}


///////////////////////////////////////////////////////////////////////////////////////////////////


CoverageAnnotator::CoverageAnnotator(const string &pathToInsertSizeDistrParams)
    : loadedInsertSizeParams(false), windowSize(50), minAnchorSize(3)
{
    if (pathToInsertSizeDistrParams.length() > 0) {
        loadedInsertSizeParams = DistrParams::loadFromFile(expectedParams, pathToInsertSizeDistrParams);
        if (loadedInsertSizeParams) {
            lowerBound = expectedParams.mean - expectedParams.std*3;
            upperBound = expectedParams.mean + expectedParams.std*3;
        }
    }
    //spanningWindow.resize(windowSize*2, 0);
}


void CoverageAnnotator::annotate(Fusion &f)
{
    using namespace boost::accumulators;

    vector<int> insertSize;
    unordered_map<int, vector<BreakpointCandidatePtr> > uniqueReadStarts, uniqueReadEnds;
    int numPaired = 0, numPAP = 0, numSpan = 0;

    accumulator_set<double, stats<tag::mean, tag::variance > > acc;

    FusionInterval fIv1 = f.getEncompassingInterval(0);
    FusionInterval fIv2 = f.getEncompassingInterval(1);
    //spanningWindow.assign(windowSize*2, 0);

    int numUniqueSplitReads = 0;

    foreach_ (const BreakpointCandidatePtr& bp, f.getBreakpoints() ) {

        if (bp->isSeparatedByIntron()) {
            continue;
        }

        int idx1 = getAlignmentIdx(bp, fIv1);

        const QueryAlignment& aln1 = bp->alns[idx1];
        const QueryAlignment& aln2 = bp->alns[1 - idx1];

        if ( bp->isOriginLocal() ) {

            if (!bp->isMultimapped()) {
                numUniqueSplitReads++;
            }

            int read_start = aln1.refStart - fIv1.begin;
            int read_end = aln2.refStart - fIv2.begin + aln2.length;

            if (fIv1.direction == Cluster::DirectionRight ) {
                int end_pos = read_start + aln1.length;
                read_start = fIv1.length() - end_pos;
            }

            if (fIv2.direction == Cluster::DirectionLeft) {
                int start_pos = read_end - aln2.length;
                read_end = fIv2.length() - start_pos;
            }


            /*int window_start = fIv1.length() - windowSize;
            int startIdx = read_start - window_start;
            if (startIdx < 0) {
                startIdx = 0;
            }
            for (int idx = startIdx; idx < windowSize; ++idx) {
                spanningWindow[idx] += 1;
            }
            int endIdx = read_end < windowSize ? read_end : windowSize - 1;
            for (int idx = windowSize; idx < endIdx; ++idx) {
                spanningWindow[idx] += 1;
            }*/

            read_end += fIv1.length();

            /*if (f.getIndex() == 134) {
                cout << bp->readName << " : " << read_start << endl;
            }*/

            uniqueReadStarts[read_start].push_back( bp );
            uniqueReadEnds[read_end].push_back( bp );

            float splitPos = (aln1.length - minAnchorSize) / (float) ( aln1.length + aln2.length - 2*minAnchorSize  );
            acc(splitPos);

            ++numSpan;

        } else {

            assert(bp->isOriginPaired());
            numPaired++;

            int read1_start = aln1.refStart - fIv1.begin;
            int read1_end = read1_start + aln1.length;

            if (fIv1.direction == Cluster::DirectionRight) {
                read1_start = fIv1.length() - read1_end;
                read1_end = read1_start + aln1.length;
            }

            int read2_start = aln2.refStart - fIv2.begin;
            int read2_end = read2_start + aln2.length;

            if (fIv2.direction == Cluster::DirectionLeft) {
                read2_start = fIv2.length() - read2_end;
                read2_end = read2_start + aln2.length;
            }

            read2_start += fIv1.length();
            read2_end += fIv1.length();

            int pair_is = read1_start < read2_start ? read2_start - read1_end : read1_start - read2_end;
            /*if (f.getIndex() == 134) {
                cout << bp->readName << " " << pair_is << endl;
            }*/

            assert(pair_is < 100000);

            insertSize.push_back( pair_is );

            if (pair_is >= lowerBound && pair_is <= upperBound) {
                numPAP++;
            }
        }
    }

    if (numSpan > 0) {

        {
            float uniqueStartsRate = uniqueReadStarts.size() / (float) numSpan;
            float uniqueEndsRate = uniqueReadEnds.size() / (float) numSpan;

            stringstream ss;
            ss << uniqueStartsRate << ";" << uniqueEndsRate << ";" << numUniqueSplitReads;
            f.setAnnotationValue(ANNOTATION_NUM_UNIQUE_STARTS, ss.str());
        }

        {
            stringstream ss;
            ss << mean(acc) << ";" << sqrt(variance(acc));

            f.setAnnotationValue(ANNOTATION_SPLIT_POS_STATS, ss.str());
        }

    }

    /*float cov1 = 0, cov2 = 0;

    for (int i = 0; i < windowSize; ++i) {
        cov1 += spanningWindow[i];
    }
    cov1 /= windowSize;

    for (int i = 0; i < windowSize; ++i) {
        cov2 += spanningWindow[i];
    }
    cov2 /= windowSize;

    stringstream ss;
    ss << cov1 << ";" << cov2;
    f.setAnnotationValue(ANNOTATION_SPAN_COVERAGE, ss.str());*/


    if (loadedInsertSizeParams) {

        sort(insertSize.begin(), insertSize.end());

        DistrParams params;
        float papRate = 1;

        if (numPaired > 0) {
            DistrParams::calcParamsFromSortedDistr(params, insertSize);
            papRate = numPAP/ (float) numPaired;
        }

        stringstream ss;
        ss << params << ";" << " papRate=" << papRate;
        f.setAnnotationValue(ANNOTATION_INSERT_SIZE, ss.str() );
    }


}


///////////////////////////////////////////////////////////////////////////////////////////////////


static GenomicInterval extractSpliceCiteInterval(const FusionInterval& iv) {
    GenomicInterval gi;
    gi.refId = iv.refId;

    if (iv.direction == Cluster::DirectionRight ) {
        gi.begin = iv.breakpointPos - 2;
        gi.end = iv.breakpointPos - 1;
    } else {
        gi.begin = iv.breakpointPos + 1;
        gi.end = iv.breakpointPos + 2;
    }

    return gi;

}

bool SpliceCiteAnnotator::loadReference(const string &path)
{

    ref.reset( new ReferenceSeqIndexed());
    return ref->load(path);
    return true;

}

void SpliceCiteAnnotator::annotate(Fusion &f)
{
    FusionInterval iv1 = f.getEncompassingInterval(0);
    FusionInterval iv2 = f.getEncompassingInterval(1);

    /*if (f.getIndex() == 99) {
        cout << "This is the guy!" << endl;
    }*/

    GenomicInterval gi1 = extractSpliceCiteInterval(iv1);
    GenomicInterval gi2 = extractSpliceCiteInterval(iv2);

    CharString str1 = ref->getSubSequence(gi1.refId, gi1.begin, gi1.length());
    CharString str2 = ref->getSubSequence(gi2.refId, gi2.begin, gi2.length());

    //cout << f.getIndex() << " " <<  toCString(str1) << " " << toCString(str2) << endl;

    int valid = 0;
    /*if ( (str1 == "GT" && str2 == "AG") || (str1 == "AG" && str2 == "GT") ||
         (str1 == "AC" && str2 == "CT" )  || (str1 == "CT" && str2 == "AC") ||
         (str1 == "GT" && str2 == "CT") || (str1 == "CT" && str2 == "GT") ||
         (str1 == "AC" && str2 == "AG") || (str1 == "AG" && str2 == "AC") )*/

    if ( ( (str1 == "GT" || str1 == "AC") && (str2 == "AG" || str2 == "CT") ) ||
         ((str1 == "AG" || str1 == "CT") && (str2 == "GT" || str2 == "AC") ) )
    {
        valid = 1;
    }

    stringstream ss;
    ss << valid << ";" << str1 << ";" << str2;
    f.setAnnotationValue(ANNOTATION_SPLICE_TYPE, ss.str());


}



///////////////////////////////////////////////////////////////////////////////////////////////////

void AnnotateFusionsTask::createGeneOverlapper()
{

    const TranscriptSet& transcripts = geneModels.getTranscripts();

    for (auto iter = transcripts.begin(); iter != transcripts.end(); ++iter) {
        const TranscriptPtr& t = iter->second;
        transcriptOverlapper.insertInterval(t->getRefName(), t->getStartPos(), t->getEndPos(), t );
        const vector<ExonPtr>& exons = t->getExons();
        // construct introns
        auto startIter = exons.begin();
        auto endIter = exons.end();
        auto prevIter = exons.begin();

        for (auto iter = startIter; iter != endIter; ++iter) {
            const ExonPtr& exon = *iter;
            exonOverlapper.insertInterval(t->getRefName(), exon->startPos, exon->endPos(), exon);
            if (iter != startIter) {
                const ExonPtr& prevExon = *prevIter;
                RegionDesc desc;
                desc.geneName = t->getGeneName();
                desc.transcriptName = t->getName();
                desc.transcriptStrand = t->isPositiveStranded() ? '+' : '-';
                desc.bioType = t->getGeneBioType();
                // We assume that exons are sorted by exon number like in Ensemble GTF
                SimpleInterval intron;
                if (exon->startPos > prevExon->startPos) {
                    intron.begin = prevExon->endPos() + 1;
                    intron.end = exon->startPos - 1;
                } else {
                    intron.begin = exon->endPos() + 1;
                    intron.end = prevExon->startPos - 1;
                }
                desc.exonId = prevExon->id;
                desc.secondExonId = exon->id;
                intronOverlapper.insertInterval(t->getRefName(), intron.begin, intron.end, desc);
                prevIter = iter;
            }

        }
    }


}


bool AnnotateFusionsTask::loadGeneExpression()
{
    ifstream infile(config.geneExpressionPath);

    if (!infile.is_open()) {
        return false;
    }

    map<string,int> geneTranscriptCount;

    string line;
    while (getline(infile,line)) {
        if (StringUtils::trim(line).length() == 0 || line[0] == '#' ) {
            continue;
        }

        vector<string> items = StringUtils::split(line, '\t');
        if (items.size() < 5) {
            continue;
        }

        string geneName = items[1];
        bool ok = false;
        float exprVal = StringUtils::parseNumber<float>(items[4], ok);
        if (!ok) {
            continue;
        }

        geneExpression[geneName] += exprVal;
        geneTranscriptCount[geneName] += 1;

   }

    for (auto iter = geneExpression.begin(); iter!= geneExpression.end(); ++iter) {

        int numTranscripts = geneTranscriptCount[iter->first];
        if (numTranscripts == 0) {
            continue;
        }
        iter->second = iter->second / (float)numTranscripts;
    }

    return true;
 }



bool AnnotateFusionsTask::init()
{
    cerr << "Loading fusions..." << endl;

    if ( !Fusion::readFusionClusters(fusions, config.inputPath) ) {
        cerr << "Failed to read fusions from " << config.inputPath << endl;
        return false;
    }


    if (config.pathToGeneModels.length() > 0) {

        cerr << "Loading transcript annotations..." << endl;

        if (! geneModels.loadTranscripts(config.pathToGeneModels)) {
            cerr << "Failed to load gene models from " << config.pathToGeneModels << endl;
            return false;
        }

        if (config.geneExpressionPath.size() > 0) {
            cerr << "Loading expression data..." << endl;
            if ( !loadGeneExpression() ) {
                cerr << "WARNING: Failed to load gene expression data from " << config.geneExpressionPath << endl;
                geneExpression.clear();
            }
        }



        cerr << "Creating annotators..." << endl;

        createGeneOverlapper();

        if (config.pathToTranscripts.length() > 0) {
            homologyAnnotator.reset( new HomologyAnnotator(config.pathToTranscripts));
        }

        if (config.pathToFusionSeqAlignments.length() > 0) {
            homologousGenesAnnotator.reset( new HomologousGenesAnnotator(config.pathToFusionSeqAlignments) );
        }

        if (config.analyzeStrandness) {
            strandnessAnnotator.reset( new StrandnessAnnotator() );
        }


        if (config.pathToGenomeFile.size() > 0) {

            spliceCiteAnnotator.reset( new SpliceCiteAnnotator() );

            if (!spliceCiteAnnotator->loadReference(config.pathToGenomeFile)) {
                cerr << "Loading genome reference..." << endl;
                return false;
            }

        }

    }

    if ( config.performCoverageAnalysis ) {
        coverageAnnotator.reset ( new CoverageAnnotator(config.pathToInsertSizeDistrParams));
    }

    return true;
}


static bool checkIfBreakOnExonBoundary(int& bPos, const ExonSet&  overalppingExons, int fuzzyBpSize ) {

    foreach_ (const ExonPtr& exon, overalppingExons) {
        SimpleInterval iv1(exon->startPos - fuzzyBpSize, exon->startPos + fuzzyBpSize);
        //Adapt bp pos here
        if (iv1.contains(bPos)) {
            bPos = exon->startPos;
            return true;
        }
        SimpleInterval iv2(exon->endPos() - fuzzyBpSize, exon->endPos() + fuzzyBpSize);
        if (iv2.contains(bPos)) {
            bPos = exon->endPos();
            return true;
        }
    }


    return false;
}


void AnnotateFusionsTask::setExpressionValue(Fusion& f, const string& geneName, const string& annId)
{
    auto iter = geneExpression.find(geneName);
    if (iter != geneExpression.end() ){
        f.setAnnotationValue(annId, boost::lexical_cast<std::string>(iter->second));
    }

}


bool AnnotateFusionsTask::overlapFusionIntervalWithGenes(Fusion& fusion, int idx)
{

    static const string GENE_1(ANNOTATION_GENE_1), GENE_2(ANNOTATION_GENE_2), EXPR_1(ANNOTATION_EXPR_1), EXPR_2(ANNOTATION_EXPR_2);

    bool transcriptPositiveStranded = true, reverse =false;
    FusionInterval& iv = fusion.getEncompassingInterval(idx);
    std::vector<ExonPtr> results;
    exonOverlapper.findIntervals(results, iv.refId, iv.begin, iv.end );

    std::vector<RegionDesc> intronResults;
    intronOverlapper.findIntervals(intronResults, iv.refId, iv.begin, iv.end);

    std::vector<TranscriptPtr> transcripts;
    transcriptOverlapper.findIntervals(transcripts, iv.refId, iv.begin, iv.end );

    foreach_ (ExonPtr exon, results) {
        iv.overalppingExons.insert(exon);
    }

    foreach_ (TranscriptPtr t, transcripts) {
        if (!t->isPositiveStranded()) {
            transcriptPositiveStranded = false;
        }
        iv.overlappingGenes.insert(t->getGene());
    }


    if (config.writeGeneAnnotations) {

        sort(intronResults.begin(), intronResults.end(), RegionDescCompare());

        const string& geneId = idx == 0 ? GENE_1 : GENE_2;
        const string& exprId = idx == 0 ? EXPR_1 : EXPR_2;
        if (results.size() > 0) {
            /*if (fusion.getIndex() == 11976) {
                cerr<< "Detected!" << endl;
            }*/
            bool breakOnExonBoundary = checkIfBreakOnExonBoundary(iv.breakpointPos, iv.overalppingExons, 3);
            string transcriptStr = GenomicAnnotation::exonToString(iv.overalppingExons, breakOnExonBoundary);
            fusion.setAnnotationValue(geneId, transcriptStr );
            setExpressionValue(fusion, results.front()->getTranscript()->getGeneName(), exprId);
            if (idx == 0 && transcripts.size() > 0) {

                bool breakpointCloseToIntervalStart = abs(iv.begin - iv.breakpointPos) < abs(iv.end - iv.breakpointPos);

                if  ( (transcriptPositiveStranded && breakpointCloseToIntervalStart) ||
                     (!transcriptPositiveStranded && !breakpointCloseToIntervalStart ) ) {
                    reverse = true;
                }
            }

        } else if (intronResults.size() > 0) {
            string intronStr = GenomicAnnotation::intronToString(intronResults);
            fusion.setAnnotationValue( geneId, intronStr);
            setExpressionValue(fusion, intronResults.front().geneName, exprId);
        } else {
            fusion.setAnnotationValue(geneId, GenomicAnnotation::INTERGENIC_REGION );
        }

    }

    return reverse;
}

void AnnotateFusionsTask::inferFusionType(Fusion &f)
{
    if (f.getEncompassingInterval(0).refId != f.getEncompassingInterval(1).refId) {
        f.setType(FusionType_InterChromosomal);
    } else {
        f.setType(FusionType_IntraChromosomal);

    }


}

void AnnotateFusionsTask::detectFusionGenesAdjacency(Fusion &f)
{
    if (f.getType() == FusionType_InterChromosomal) {
        return;
    }
    assert(f.getType() == FusionType_IntraChromosomal);
    const FusionInterval& fIv1 = f.getEncompassingInterval(0);
    const FusionInterval& fIv2 = f.getEncompassingInterval(1);
    const string& refId = fIv1.refId;

    // TODO: when no overlapping genes are found use just an interval instead?
    // TODO: check if the adjacency occurs between last exon of 5'gene and first exon 3'gene

    foreach_ (GenePtr gene1, fIv1.overlappingGenes) {
        foreach_ (GenePtr gene2, fIv2.overlappingGenes ) {
            SimpleInterval gIv1(gene1->getStartPos(), gene1->getEndPos());
            SimpleInterval gIv2(gene2->getStartPos(), gene2->getEndPos());
            if (gIv1.intersects(gIv2)) {
                f.setType( FusionType_ReadThroughIntersect );
                return;
            } else {
                SimpleInterval ivBetweenGenes;
                if (gIv1.begin < gIv2.end) {
                    ivBetweenGenes.begin = gIv1.end + 1;
                    ivBetweenGenes.end = gIv2.begin - 1;
                } else {
                    ivBetweenGenes.begin = gIv2.end + 1;
                    ivBetweenGenes.end = gIv1.begin - 1;
                }

                vector<TranscriptPtr> res;
                transcriptOverlapper.findIntervals(res, refId, ivBetweenGenes.begin, ivBetweenGenes.end);
                if (res.size() == 0) {
                    f.setType(  FusionType_ReadThroughAdjacent );
                    return;
                }
            }
        }


    }




}


static void swapCommaSeparatedAnnotation(Fusion& f, const string& name) {
    if (f.hasAnnotation(name)) {
        string val = f.getAnnotationValue(name);
        vector<string> elems = StringUtils::split(val, ';');
        string newVal = elems[1] + ";" + elems[0];
        f.setAnnotationValue(name, newVal);
    }
}


static void swapAdditionalAnnotations(Fusion& f) {
    string val = f.getAnnotationValue(ANNOTATION_METACLUSTER_PROPS);
    if (!val.empty()) {
        vector<string> elems = StringUtils::split(val, ';');
        string newVal = elems[2] + ";" + elems[3] + ";" + elems[0] + ";" + elems[1];
        f.setAnnotationValue(ANNOTATION_METACLUSTER_PROPS, newVal);
    }

}


static void swapFusionIntervals(Fusion& f) {

    std::vector<FusionInterval> reverseIntervals;
    reverseIntervals.push_back(f.getEncompassingInterval(1));
    reverseIntervals.push_back(f.getEncompassingInterval(0));
    f.setIntervals(reverseIntervals);


    std::vector<BreakpointCandidatePtr> candidates = f.getBreakpoints();
    foreach_ (BreakpointCandidatePtr& c, candidates) {
        c->swapAlignments();
    }

}

int AnnotateFusionsTask::run()
{
    if (!init()) {
        return -1;
    }

    cerr << "Annotating fusions.." << endl;

    foreach_ (Fusion& f, fusions) {

        bool swapFusionSegments = false;

        inferFusionType(f);

        if (!geneModels.isEmpty()) {
            swapFusionSegments = overlapFusionIntervalWithGenes(f, 0);
            overlapFusionIntervalWithGenes(f, 1);

            detectFusionGenesAdjacency(f);
            if (homologyAnnotator) {
                homologyAnnotator->annotate(f);
            }
            if (homologousGenesAnnotator) {
                homologousGenesAnnotator->annotate(f);
            }
            if (strandnessAnnotator) {
                strandnessAnnotator->annotate(f);
            }
            if (spliceCiteAnnotator) {
                spliceCiteAnnotator->annotate(f);
            }


        }

        if (coverageAnnotator) {
            coverageAnnotator->annotate(f);
        }


        if (swapFusionSegments) {
            // Known strand is applied to form correct 5'-3' order of fusion
            swapFusionIntervals(f);
            f.swapAnnotationValues(ANNOTATION_GENE_1, ANNOTATION_GENE_2);
            f.swapAnnotationValues(ANNOTATION_EXPR_1, ANNOTATION_EXPR_2);
            swapCommaSeparatedAnnotation(f, ANNOTATION_FUSION_COVERAGE);
            swapCommaSeparatedAnnotation(f, ANNOTATION_STRANDNESS);
            swapAdditionalAnnotations(f);
        }

    }

    cerr << "Saving results..." << endl;

    if (!Fusion::writeFusionClusters(fusions,config.outputPath, false)) {
        cerr << "Failed to write updated clusters to " << config.outputPath << endl;
        return -1;
    }

    return 0;
}



