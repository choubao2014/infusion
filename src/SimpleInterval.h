#ifndef SIMPLE_INTERVAL_H
#define SIMPLE_INTERVAL_H

struct SimpleInterval {
    int begin;
    int end;

    SimpleInterval() : begin(-1), end(-1) {}
    SimpleInterval(int i1, int i2) : begin(i1), end(i2) {}
    int length() const { return end - begin + 1; }
    bool intersects(const SimpleInterval& other) const {
        int sd = begin - other.begin;
        return (sd >= 0) ? (sd < other.length()) : (-sd < length());
    }

    bool contains(int pos) const {
        return ( pos >= begin && pos <= end );
    }

    bool contains(const SimpleInterval& other) const {
        return ( contains(other.begin) && contains(other.end) );
    }

    bool operator== (const SimpleInterval& other) const {
        return (begin == other.begin && end == other.end );
    }

    bool operator!= (const SimpleInterval& other) const {
        return (begin != other.begin || end != other.end );
    }

    int getMidPoint() const { return ( (begin + end) / 2 ); }

    bool operator< (const SimpleInterval& other) const {
        if (begin == other.begin) {
            return length() < other.length();
        } else {
            return begin < other.begin;
        }

    }


};

#endif // SIMPLE_INTERVAL_H
