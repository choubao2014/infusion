#ifndef ALIGNMENTUTILS_H
#define ALIGNMENTUTILS_H

#include <string>
#include <seqan/sequence.h>

#include "GenomicInterval.h"


using std::string;
using std::vector;
using seqan::CharString;

struct AlignmentResult {
    int score;
    int percentageIdentity;
    AlignmentResult() : score(0), percentageIdentity(0) {}
};

class AlignmentUtils
{
public:

   /* Return value: alignment score in the following scheme: match 0, mismatch 2, gap 2*/
   static int alignFlankingPattern(const string& pattern, const string& source, bool searchDownstream, int tolerance);

   static int alignFlankingPattern(const string& pattern, const string& source, int startPos, int tolerance);

   static AlignmentResult  performLocalAlignment(const CharString& host, const CharString& query);

   static const GenomicInterval& pickNearestToBreakpointInterval(const vector<GenomicInterval>& intervals, bool firstOfPair, bool transcriptIsPositve ) {
       if (firstOfPair) {
           return transcriptIsPositve ? intervals.back() : intervals.front();
       } else {
           return !transcriptIsPositve ? intervals.front() : intervals.back();
       }
   }


   //static void performLocalAlignment(AlignmentResult& result, const CharString& host, const CharString& query);

};

#endif // ALIGNMENTUTILS_H
