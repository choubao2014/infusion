#include <iostream>
#include <vector>

#include <seqan/bam_io.h>
#include <seqan/index.h>
#include <seqan/arg_parse.h>

#include "StringUtils.h"
#include "Fusion.h"
#include "CreateFinalAlignmentTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "output_fusion_alignments"



int getRefIdByName(const string& refName,  const  StringSet<CharString> &refNames ) {

    for (int i =0, sz = length(refNames); i < sz; ++i ) {
        if (refNames[i] == refName.c_str()) {
            return i;
        }
    }

    return -1;
}



BamAlignmentRecord toBamAlignmentRecord(int fusionIdx, Cluster::Direction dir, const BreakpointCandidatePtr& bp, const QueryAlignment& aln, const  StringSet<CharString> &refNames ) {
    BamAlignmentRecord rec;

    rec.qName = bp->readName;
    rec.rID = getRefIdByName(aln.refId, refNames);
    rec.beginPos = aln.refStart;

    if (aln.queryStart != 0) {
        CigarElement<> el('H', aln.queryStart);
        append(rec.cigar, el);
    }

    CigarElement<> mapped('M', aln.length);
    append(rec.cigar, mapped);

    rec.flag = 0;
    if (! aln.positiveStrand) {
        rec.flag |= BAM_FLAG_RC;
    }


    CharString samTags;
    samTags += "XF:i:";
    samTags += StringUtils::numToStr(fusionIdx).c_str();

    samTags += "\tXO:Z:";
    samTags += bp->originToStr();

    samTags += "\tXD:Z:";
    samTags += Cluster::directionToChar(dir);

    assignTagsSamToBam(rec.tags, samTags);
    //rec.tags += originTag;


    //BamTagsDict tags(rec.tags);
    //setTagValue(tags, "XF", "f", fusionId);
    //setTagValue(tags, "XF", "o", bp->originToStr().c_str());


    return rec;
}

struct ReadInfo {
    int startPos, endPos;
    string name;
};


int performQuickAnalysis(const CreateFinalAlignmentTaskConfig& cfg) {

    cerr << "Loading fusions..." << endl;

    vector<Fusion> fusions;

    if ( !Fusion::readFusionClusters(fusions, cfg.fusionsPath ) ) {
        cerr << "Failed to read fusions from " << cfg.fusionsPath << endl;
        return -1;
    }


    BamStream bamStreamIn(cfg.headerBamPath.c_str(), BamStream::READ);
    if (!isGood(bamStreamIn)) {
        cerr << "Failed to open input alignment file: " << cfg.headerBamPath << endl;
        return -1;
    }



    BamStream alnOutStream;
    open(alnOutStream, cfg.outputPath.c_str(),BamStream::WRITE, BamStream::BAM);
    if (!isGood(alnOutStream)) {
        cerr << "Failed to create out file for  alignments: " << cfg.outputPath << endl;
        return -1;
    }

    alnOutStream.header = bamStreamIn.header;
    const StringSet<CharString>& refNames = bamStreamIn._nameStore;

    cerr << "Writing alignments..." << endl;



    foreach_ (const Fusion& f, fusions) {
        vector<ReadInfo> fusionReads;
        const std::vector<BreakpointCandidatePtr>& breakpoints = f.getBreakpoints();
        foreach_ (const BreakpointCandidatePtr& bp, breakpoints ) {
            if (cfg.outputReadClusters) {

                if (bp->isOriginLocal()) {

                    f.getEncompassingInterval(0);
                    f.getEncompassingInterval(1);
                } else {
                    ReadInfo r1,r2;
                    const QueryAlignment aln1 = bp->alns[0];
                    r1.startPos = aln1.refStart;
                    r1.endPos = aln1.inRefEnd();
                    assert(r1.startPos < r2.endPos);
                    fusionReads.push_back(r2);
                    const QueryAlignment aln2 = bp->alns[1];
                    r2.startPos = aln2.refStart;
                    r2.endPos = aln2.inRefEnd();
                    assert(r2.startPos < r2.endPos);
                    fusionReads.push_back(r1);


                }



            }

            for (int i = 0; i < 2; ++i ) {
                BamAlignmentRecord  rec = toBamAlignmentRecord(f.getIndex(), f.getDirection(i), bp, bp->alns.at(i), refNames);
                int res = writeRecord(alnOutStream, rec);
                if (res != 0) {
                    std::cerr << "Error writing alignment" << rec.qName << endl;
                }
            }

        }
    }

    close(alnOutStream);

    cerr << "Finished." << endl;

    return 0;
}


void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It outputs the spliced alignments including those supporting fusions in BAM format. \
                   There is a quick mode (output no reads) and advanced mode which outputs all alignments along with reads");

    string usageLine;
    usageLine.append(" [OPTIONS] FUSION_CLUSTERS");
    addUsageLine(parser, usageLine);

    addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "fusionClusters.txt"));

    std::vector<string> extentions;
    setValidValues(parser, 0 , extentions );

    addSection(parser, "Options");

    addOption(parser, ArgParseOption("ibam", "input-bam", "BAM file to take header from.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "o", "output", "Path to output file with alignments", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "t", "transcriptomic", "Path to transcriptomic (and genomic) alignment", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "l", "local", "Path to local alignments", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "gtf", "annotations", "Path to gene annotations in Ensebml GTF", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption("q", "quick", "Output alignments of reads supporting fusions, without read sequences"));
    addOption(parser, ArgParseOption( "s", "sam", "Path output in SAM format, default is BAM." ));


    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    bool quickMode = false;

    CreateFinalAlignmentTaskConfig settings;

    string localAlignmentsPath;
    getArgumentValue(settings.fusionsPath, parser, 0);
    getOptionValue(quickMode, parser, "q");
    getOptionValue(settings.headerBamPath, parser, "ibam");
    getOptionValue(settings.outputPath, parser, "o");
    getOptionValue(settings.transcriptomicAlignmentsPath, parser, "t");
    getOptionValue(localAlignmentsPath, parser, "l");
    getOptionValue(settings.annotationsPath, parser, "gtf");
    getOptionValue(settings.useSamFormat, parser, "s");

    if (settings.outputPath.length() == 0) {
        cerr << "The output file is not set!" << endl;
        return -1;
    }

    settings.localAlignmentsPath = StringUtils::split(localAlignmentsPath,',');

    if (settings.localAlignmentsPath.size() == 0) {
        cerr << "The input BAM file with local alignments is not set!" << endl;
        return -1;
    }

#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input fusions file: " << settings.fusionsPath << endl;
    cerr << "Output alignment file: " << settings.outputPath << endl;
#endif

    settings.headerBamPath = settings.localAlignmentsPath.front();

    if (quickMode) {
        return performQuickAnalysis(settings);
    } else {
        CreateFinalAlignmentTask t(settings);
        return t.run();
    }

}



