#ifndef MERGE_INTRON_SEPARATED_FUSIONS_TASK_H
#define MERGE_INTRON_SEPARATED_FUSIONS_TASK_H

#include "Fusion.h"
#include "GenomicOverlapDetector.h"

class FusionSegment;
typedef boost::shared_ptr<FusionSegment> FusionSegmentPtr;


class FusionSegment {
    Fusion& parent;
    FusionSegmentPtr partner;
    int intervalIndex;
public:
    FusionSegment(Fusion& f, int idx) : parent(f), intervalIndex(idx) {}
    const Fusion& getFusion() const { return parent; }
    int getIntervalIdx() const { return intervalIndex; }
    Fusion& getFusion() { return parent; }
    const FusionSegmentPtr& getPartner() const { return partner; }
    void setPartner(const FusionSegmentPtr& fs) { partner = fs; }
    Cluster::Direction getDirection() const { return parent.getEncompassingInterval(intervalIndex).direction; }
    const FusionInterval& getEncompassingInterval() const { return parent.getEncompassingInterval(intervalIndex); }
    int getBreakpointPos() const { return parent.getBreakpointPos(intervalIndex); }
};



struct MergeIntronSeparatedFusionsTaskConfig {
    string inputPath, outputPath;
    int maxIntronLength, meanInsertSize;
    MergeIntronSeparatedFusionsTaskConfig() : maxIntronLength(20000), meanInsertSize(0) {}
};



class MergeIntronSeparatedFusionsTask
{
    MergeIntronSeparatedFusionsTaskConfig config;
    vector<Fusion> fusions;
    GenomicOverlapDetector<FusionSegmentPtr> fusionOverlapDetector;
    void joinFusionsSeparatedByIntron(FusionSegmentPtr& destFusionSeg, FusionSegmentPtr& toJoinFusionSeg);
    bool canBeMerged(const FusionSegmentPtr& curSegment,const FusionSegmentPtr& nextSegment);
    inline bool segmentsWithinMaxIntronLength(const FusionSegmentPtr& curSegment,const FusionSegmentPtr& nextSegment);
    inline bool intervalsWithinMaxIntronLength(const GenomicInterval& iv1, const GenomicInterval& iv2);
    bool haveCompatiblePartners(const FusionSegmentPtr& segment1, const FusionSegmentPtr& segment2);
    void mergeIntronSepartedFusions();
    void createFusionOverlapDetector();
public:
    MergeIntronSeparatedFusionsTask(const MergeIntronSeparatedFusionsTaskConfig& cfg) : config(cfg) {}
    int run();
};



#endif // MERGE_INTRON_SEPARATED_FUSIONS_TASK_H
