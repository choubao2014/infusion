#include <iostream>
#include <cassert>

#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/misc/misc_interval_tree.h>

#include "Global.h"
#include "SimpleInterval.h"
#include "IntervalTree.h"

using namespace std;
using seqan::String;

typedef seqan::IntervalAndCargo<int,size_t> SInterval;

void generateRandomIntervals(int low, int high, int numIntervals, int intervalSize, vector<SimpleInterval>& results) {

    for (int i = 0; i < numIntervals; ++i) {
        int range = high - low + 1;
        int start = rand() % range + low;
        int end = start + intervalSize - 1;
        results.push_back(SimpleInterval(start, end));
    }
}

void testIntervalTree() {

    IntervalTree<SimpleInterval> intervalTree;

    vector<SimpleInterval> source, queries;
    srand(clock());
    generateRandomIntervals(1, 1000000, 10000, 1000, source );
    generateRandomIntervals(1, 1000000, 1000000, 75, queries );

    seqan::String<SInterval> intervals;
    seqan::resize(intervals, source.size());

    for ( size_t idx = 0, count = source.size(); idx < count; ++idx) {
        SimpleInterval iv = source.at(idx);
        intervalTree.insertInterval( iv );
        SInterval siv;
        siv.i1 = iv.begin;
        siv.i2 = iv.end + 1;
        siv.cargo = idx;
        intervals[idx] = siv;
    }

    seqan::IntervalTree<int,size_t> seqanTree(intervals);

    clock_t t0 = 0, t1 = 0, t2 = 0, t3 = 0;

    clock_t s0 = clock();

    int numFails = 0;

    foreach_ (SimpleInterval& query, queries) {

        vector<SimpleInterval> list1, list2;

        clock_t s1 = clock();
        foreach_ (SimpleInterval& iv, source) {
            if (iv.intersects(query)) {
                list1.push_back(iv);
            }
        }

        t1 += clock() - s1;

        clock_t s2 = clock();

        intervalTree.findOverlapers(list2,query);

        t2 += clock() - s2;


        clock_t s3 = clock();

        String<size_t> res;
        seqan::findIntervals(seqanTree, query.begin, query.end + 1, res);

        t3 += clock() - s3;


        // COMPARE
        size_t requiredSize = list1.size();

        if ( requiredSize  != list2.size() ) {
            assert(0);
            cout << "InFusion interval search failed!" << endl;
        }

        if (requiredSize != seqan::length(res) ) {
           ++numFails;
        }
    }

    t0 = clock() - s0;

    cout << "Overall time: " << (t0 / (double) CLOCKS_PER_SEC) << endl;
    cout << "Linear time: " << (t1 / (double) CLOCKS_PER_SEC) << endl;
    cout << "Interval tree time: " << (t2 / (double) CLOCKS_PER_SEC) << endl;
    cout << "Seqan tree time: " << (t3 / (double) CLOCKS_PER_SEC) << endl;

    cout << "Seqan fails:" << numFails << endl;
}

int main(int argc, char const ** argv)
{

#ifdef DEBUG

    cout << "Running in DEBUG mode" << endl;

#else
    cout << "Running in RELEASE mode" << endl;

#endif //DEBUG

    testIntervalTree();

    return 0;
}

