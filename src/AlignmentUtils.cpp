#include <seqan/basic.h>
#include <seqan/score.h>
#include <seqan/align.h>

#include "AlignmentUtils.h"

using namespace std;
using namespace seqan;

int AlignmentUtils::alignFlankingPattern(const string &pattern, const string &source, bool searchDownstream, int tolerance)
{

    int fromPos = searchDownstream ? source.length() - pattern.length() : 0;

    return alignFlankingPattern(pattern, source, fromPos, tolerance);
}

int AlignmentUtils::alignFlankingPattern(const string &pattern, const string &source, int fromPos, int tolerance)
{
    CharString seq1(pattern);
    int s2Len = pattern.length() + tolerance;
    CharString seq2( source.substr(fromPos,s2Len ));
    typedef Align<CharString,ArrayGaps> TAlign;      // align type
    TAlign align;

    resize(rows(align), 2);
    assignSource(row(align,0),seq1);
    assignSource(row(align,1),seq2);

    int score = globalAlignment(align, Score<int,Simple>(0,-1,-1), AlignConfig<true,false,false,true>() );

    //cout << align << endl;

    return score;

}

AlignmentResult AlignmentUtils::performLocalAlignment(const CharString &host, const CharString &query)
{

    Align<CharString> align;
    resize(rows(align), 2);
    assignSource(row(align, 0), host);
    assignSource(row(align, 1), query);


    //TODO: figure out scoring!
    SimpleScore scoringScheme(2, -1, -2, -1);
    int score = localAlignment(align, scoringScheme);

    AlignmentResult res;
    res.score = score;
    res.percentageIdentity = (score* 100) /  (length(query)*2);

    //string qString(toCString(query));
    //cerr << "Query length: " << qString.length() << endl;

    //std::stringstream ssHost, ssQuery;
    //ssHost << row(align, 0);
    //ssQuery << row(align, 1);
    //cerr << "Score is " << res.score << endl << ssHost.str() << endl << ssQuery.str() << endl << endl;

    return res;

}

/*void AlignmentUtils::performLocalAlignment(AlignmentResult &result, const CharString &host, const CharString &query)
{
    Align<const CharString> align;
    resize(rows(align), 2);
    assignSource(row(align, 0), host);
    assignSource(row(align, 1), query);

    SimpleScore scoringScheme(2, -1, -2);
    result.score = localAlignment(align, scoringScheme);
}*/
