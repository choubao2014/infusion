#ifndef MISCUTILS_H
#define MISCUTILS_H

#include <algorithm>
#include <set>
#include <map>
#include <memory>
#include <boost/shared_ptr.hpp>
#include <boost/functional/hash.hpp>




class MiscUtils
{
public:

template <typename T, typename Container>
    static bool contains(const Container& container, const T& val) {
        return std::find(container.begin(), container.end(), val)!=container.end();
    }

template<typename multimap_t>
      static void multimapKeys(const multimap_t &m,
                std::set<typename multimap_t::key_type> &k)
      {
          typename std::set<typename multimap_t::key_type>::iterator keys_end = k.end();

          for (typename multimap_t::const_iterator b(m.begin()), e(m.end()); b != e; ++b)
          {
               k.insert(keys_end, b->first);
          }
      }

template <typename K, typename V>
      static V getMapValWithDef(const  std::map <K,V> & m, const K & key, const V & defval ) {
         typename std::map<K,V>::const_iterator it = m.find( key );
         if ( it == m.end() ) {
            return defval;
         }
         else {
            return it->second;
         }
      }


};

#endif // MISCUTILS_H
