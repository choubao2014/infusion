#ifndef GLOBAL_H
#define GLOBAL_H

#include <string>
#include <vector>
#include <map>
#include <memory>
#include <iostream>

#include <boost/foreach.hpp>


// Make it look pretty
#define foreach_   BOOST_FOREACH

using std::string;
using std::vector;
using std::map;

enum TriState {
    TrisState_Yes = 1,
    TriState_No = 0,
    TriState_Unknown = -1
};


#endif // GLOBAL_H
