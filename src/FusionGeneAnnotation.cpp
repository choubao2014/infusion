#include "FusionGeneAnnotation.h"
#include "StringUtils.h"

const string GenomicAnnotation::EXON("exon");
const string GenomicAnnotation::INTRON("intron");
const string GenomicAnnotation::INTERGENIC_REGION("intergenic_region");

GenomicAnnotation GenomicAnnotation::parseAnnotation(const string &str)
{
    GenomicAnnotation a;

    if (str.length() == 0) {
        a.featureType = "none";
        return a;
    }

    vector<string> regions = StringUtils::split(str, ';');

    foreach_ (const string& r, regions ) {

        if (r == "exonic_break") {
           a.breakOnExon = true;
           continue;
        }

        vector<string> items = StringUtils::split(r, ':');
        a.featureType = items[0];

        if (items.size() > 1) {
            RegionDesc rd;
            rd.bioType = items[1];
            rd.geneName = items[2];
            rd.transcriptName = items[3];
            rd.transcriptStrand = items[4][0];
            rd.exonId = items[5];
            if (items.size() > 6) {
                rd.secondExonId = items[6];
            }
            a.features.push_back(rd);
        }
    }

    return a;



    return a;
}

string GenomicAnnotation::exonToString(const ExonSet &exons, bool breakOnExonBoundary )
{
    string transcriptStr;

    foreach_ (const ExonPtr& exon, exons) {
        if (transcriptStr.length() > 0) {
            transcriptStr += ";";
        }
        TranscriptPtr t = exon->getTranscript();
        transcriptStr += GenomicAnnotation::EXON + ":";
        transcriptStr += t->getGeneBioType() + ":" + t->getGeneName();
        transcriptStr += ":" + t->getName() + ":";
        transcriptStr += ( t->isPositiveStranded() ? "+" : "-" );
        transcriptStr += ":" + exon->id;
    }

    if (breakOnExonBoundary) {
        transcriptStr += ";exonic_break";
    }

    return transcriptStr;

}

string GenomicAnnotation::intronToString(const vector<RegionDesc>& introns)
{
    string intronStr;
    foreach_ (const RegionDesc& desc, introns) {
            if (intronStr.length() > 0) {
                intronStr += ";";
            }
            intronStr += GenomicAnnotation::INTRON + ":";
            intronStr += desc.bioType;
            intronStr += ":" + desc.geneName;
            intronStr += ":" + desc.transcriptName + ":";
            intronStr += (desc.transcriptStrand == '+' ? "+" : "-" );
            intronStr += ":" + desc.exonId;
            intronStr += ":" + desc.secondExonId;
        }

    return intronStr;

}


string GenomicAnnotation::getTranscriptsStr() const
{
    if (features.size() > 0) {
        string transcriptNames;
        foreach_ (const RegionDesc& feature, features) {
            string tName = feature.transcriptName + ":" + feature.exonId;
            if (feature.secondExonId.length() > 0) {
                tName += ":" + feature.secondExonId;
            }
            if (transcriptNames.length() > 0) {
                transcriptNames += ";";
            }
            transcriptNames += tName;
        }
        return transcriptNames;

    } else {
        return "none";
    }
}

string GenomicAnnotation::getGenesStr() const
{
    if (features.size() > 0) {
        std::set<string> geneNames;
        foreach_(const RegionDesc& feature, features) {
            geneNames.insert(feature.geneName);
        }
        return StringUtils::join(geneNames, ";");
    } else {
        return "none";
    }
}

string GenomicAnnotation::getBioTypesStr() const
{
    if (features.size() > 0) {
        return features.at(0).bioType;
    } else {
        return "none";
    }
}

char GenomicAnnotation::getStrand() const {

    if (features.size() > 0) {
        return features.at(0).transcriptStrand;
    } else {
        return '.';
    }

}

bool GenomicAnnotation::isProteinCoding() const
{
    if (features.size() > 0) {
        if (features.at(0).bioType == "protein_coding") {
            return true;
         }
    }
    return false;
}
