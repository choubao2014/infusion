#include <iostream>
#include <vector>
#include <algorithm>

#include <seqan/arg_parse.h>

#include "Fusion.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME                    "filter_fusion_candidates"


void dumpClusters(const vector<Fusion>& fusionClusters, const string& fileName)
{
    ofstream out;
    out.open(fileName, ios_base::out);


    foreach_ (const Fusion& f, fusionClusters) {

        const vector<BreakpointCandidatePtr>& bps = f.getBreakpoints();

        for (int i = 0; i < 2; ++i) {
            vector<QueryAlignment> queryAlignments;
            SimpleInterval iv(bps.at(0)->alns[i].refStart, bps.at(0)->alns[i].inRefEnd() );
            for (size_t j = 1; j < bps.size(); ++j ) {
                const QueryAlignment& aln = bps.at(j)->alns.at(i);
                queryAlignments.push_back(aln);
                iv.begin =  aln.refStart < iv.begin ? aln.refStart : iv.begin;
                iv.end = aln.inRefEnd() > iv.end ? aln.inRefEnd() : iv.end;
            }

            sort(queryAlignments.begin(), queryAlignments.end());

            out << endl << "i ";
            for (int i = 0; i < iv.length(); ++i) {
                out << "*";
            }
            out << endl;

            foreach_(const QueryAlignment& aln, queryAlignments) {
                assert( iv.begin <= aln.refStart);
                int startPos = aln.refStart - iv.begin;
                /*if (segment->getBreakpointCandidate()->isOriginLocal()) {
                    if (segment->getBreakpointCandidate()->isRedeemed()) {
                        out << "r";
                    } else {
                        out << "l";
                    }
                } else if (segment->getBreakpointCandidate()->isOriginPaired()) {
                    out << "p ";
                } else {
                    out << "u ";
                }*/
                out << "r" << endl;
                for (int i = 0; i < iv.length(); ++i) {
                    if (i >= startPos && i < startPos + aln.length) {
                        out << "*";
                    } else {
                        out << " ";
                    }
                }
                out << endl;

            }
            out << endl;

        }

    }

}


void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. Outputs visulization of the clusters in ASCII format.");

    string usageLine;
    usageLine.append(" [OPTIONS] FUSION_CLUSTERS");
    addUsageLine(parser, usageLine);

    addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "fusionClusters.txt"));

    std::vector<string> extentions;
    setValidValues(parser, 0 , extentions );

    addSection(parser, "Options");

    addOption(parser, ArgParseOption( "out", "output", "Path to output file with filtered fusions", ArgParseArgument::STRING ));

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    string inFilePath, outFilePath;

    getArgumentValue(inFilePath, parser, 0);

    getOptionValue(outFilePath, parser, "out");

#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input file: " << inFilePath << endl;
    cerr << "Output fusions file: " << outFilePath << endl;
#endif


    vector<Fusion> fusions;
    cerr << "Loading fusions..." << endl;

    if ( !Fusion::readFusionClusters(fusions, inFilePath) ) {
        cerr << "Failed to read fusions from " << inFilePath << endl;
        return false;
    }


    dumpClusters(fusions, outFilePath);



}



