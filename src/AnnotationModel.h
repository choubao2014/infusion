#ifndef ANNOTATION_MODEL_H
#define ANNOTATION_MODEL_H

#include <string>
#include <limits>
#include <algorithm>
#include <set>

#include <boost/shared_ptr.hpp>

#include "GenomicInterval.h"
#include "Feature.h"


using std::string;

class Gene {
    string id, name, biotype;
    // 0-based, end-inclusive
    int startPos, endPos;
public:
    Gene(const string& geneId, const string& geneName, int start, int end) : id(geneId), name(geneName),
        startPos(std::min(start,end)), endPos(std::max(start,end)) { }
    const string& getName() { return name; }
    const string& getId() { return id; }
    void setBioType(const string& t) { biotype = t; }
    const string& getBioType() { return biotype; }
    int getStartPos() const { return startPos; }
    void setStartPos(int pos) { startPos = pos; }
    int getEndPos() const { return endPos; }
    void setEndPos(int pos) { endPos = pos; }
};

typedef boost::shared_ptr<Gene> GenePtr;

class Transcript;
typedef boost::shared_ptr<Transcript> TranscriptPtr;

class Exon : public FeatureData {
    TranscriptPtr transcript;
public:
    Exon(TranscriptPtr t) : transcript(t) {}
    const TranscriptPtr& getTranscript() const { return transcript; }
};

typedef boost::shared_ptr<Exon> ExonPtr;

class Transcript : protected FeatureData {
    vector<ExonPtr> exons; // Exons are sorted by coordinate
    string name;
    GenePtr gene;
    int cdrStart, cdrStop;
public:
    Transcript(const string& tId, const string& tName, const GenePtr& parentGene )
        : FeatureData(tId), name(tName), gene(parentGene), cdrStart(-1),cdrStop(-1) { assert(gene); }
    bool init();
    void addExon(const ExonPtr& exon) { exons.push_back(exon); }
    int getStartPos() const { return startPos; }
    int getLength() const { return length; }
    int getEndPos() const;
    bool isPositiveStranded() const { return positiveStranded; }

    // Coding region coordiantes are concordant with strand i.e.
    // for reverse transcript the start coord is larger than stop
    int getCdrStart() const { return cdrStart; }
    void setCdrStart(int codingRegionStart) { cdrStart = codingRegionStart; }
    int getCdrStop() const { return cdrStop; }
    void setCdrStop(int codingRegionStop) { cdrStop = codingRegionStop; }

    const string& getId() const { return id; }
    const string& getRefName() const { return seqName; }
    const string& getName() const { return name; }
    const vector<ExonPtr>& getExons() const { return exons; }
    const string& getGeneName() const;
    const string& getGeneId() const;
    const string& getGeneBioType() const;
    const GenePtr& getGene() const { return gene; }
    int getGenomicPos( int pos) const;
    int genomicCoordToTranscriptomic(const string& refName, int pos) const;
    bool geneContainsGenomicPoint(int pos) const;
    // The intervals are sorted by genomic coordinate
    vector<GenomicInterval> getGenomicIntervals(int localPos, int len) const;
    };


struct TranscriptNameSort {
   bool operator()(const ExonPtr& l, const ExonPtr& r) {
       return l->getTranscript()->getName() < r->getTranscript()->getName();
   }
};

typedef std::set< boost::shared_ptr<Exon>, TranscriptNameSort > ExonSet;

typedef map<string,TranscriptPtr> TranscriptSet;

class AnnotationModel
{
    TranscriptSet transcripts;
    int numRecords;
public:
    AnnotationModel();
    bool loadTranscripts(const string& fileName, bool useIdAsName = true);
    bool isEmpty() { return transcripts.size() == 0; }
    const TranscriptSet& getTranscripts()const { return transcripts; }
    const TranscriptPtr getTranscript(const string& transcriptName) const;

};



#endif // ANNOTATION_MODEL_H
