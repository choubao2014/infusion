#include "Fusion.h"
#include "FindSupportingMatesTask.h"
#include "StringUtils.h"

using namespace std;
using namespace seqan;

#define MAX_INSERT_SIZE 500


bool FindSupportingMatesTask::checkSecondMateInTranscript(const TranscriptPtr& transcript, const QueryAlignment& aln, bool upstreamMates, const BamAlignmentRecord& possibleMate) {


    AlignmentStrand possibleMateStrand = hasFlagRC(possibleMate) ? AlignmentStrand_Reverse : AlignmentStrand_Forward;

    AlignmentStrand alnStrandInTranscript = aln.positiveStrand == transcript->isPositiveStranded() ? AlignmentStrand_Forward : AlignmentStrand_Reverse;

    // We imply that mates protocols are unified to FORWARD-FORWARD

    if ( alnStrandInTranscript != possibleMateStrand ) {
        return false;
    }

    if (transcript->getRefName() != aln.refId) {
        return false;
    }

    SimpleInterval testRegion;

    bool strandCondition = alnStrandInTranscript == AlignmentStrand_Forward;
    if (upstreamMates == strandCondition) {
        testRegion.begin = transcript->isPositiveStranded() ? transcript->getGene()->getStartPos() : transcript->getGene()->getEndPos();
        testRegion.end = transcript->getGenomicPos(possibleMate.beginPos);
    } else {
        int alnEndPos = getAlignmentLengthInRef(possibleMate) + possibleMate.beginPos;
        testRegion.begin = transcript->getGenomicPos(alnEndPos);
        testRegion.end = transcript->isPositiveStranded() ? transcript->getGene()->getEndPos() :transcript->getGene()->getStartPos();
    }


    if (testRegion.begin > testRegion.end) {
        std::swap(testRegion.begin, testRegion.end);
    }

    SimpleInterval genomicMateInterval(aln.refStart, aln.inRefEnd());

    return genomicMateInterval.intersects(testRegion);


    /*if (upstreamMates) {
        if (alnStrandInTranscript == AlignmentStrand_Forward) {
            insertSizeRegion.begin = max(0, possibleMate.pos - MAX_INSERT_SIZE);
            insertSizeRegion.end = possibleMate.pos;
        } else {
            int alnEndPos = getAlignmentLengthInRef(possibleMate) + possibleMate.pos;
            insertSizeRegion.begin = alnEndPos;
            insertSizeRegion.end = min(alnEndPos + MAX_INSERT_SIZE, transcript->getLength() - 1);
       }
    } else {
        if (alnStrandInTranscript == AlignmentStrand_Forward) {
            int alnEndPos = getAlignmentLengthInRef(possibleMate) + possibleMate.pos;
            insertSizeRegion.begin = alnEndPos;
            insertSizeRegion.end = min(alnEndPos + MAX_INSERT_SIZE, transcript->getLength() - 1);
        } else {
            insertSizeRegion.begin = max(0, possibleMate.pos - MAX_INSERT_SIZE);
            insertSizeRegion.end = possibleMate.pos;
        }
    }*

    int gStart = transcript->getGenomicPos(insertSizeRegion.begin);
    int gEnd = transcript->getGenomicPos(insertSizeRegion.end);
    SimpleInterval insertSizeRegionProjection(min(gStart,gEnd), max(gStart,gEnd));

    SimpleInterval firstMateInterval(aln.refStart, aln.inRefEnd());

    return firstMateInterval.intersects(insertSizeRegionProjection);*/
}



bool FindSupportingMatesTask::findMatesFromAlignments(BreakpointCandidateMap &queryBreakpoints, bool upstreamMates, const string &pathToAlignments, bool transcriptomic)
{

    cerr << "Searching for possible mate pairs in file " << pathToAlignments << endl;

    ChunkBamReaderConfig readerCfg;
    readerCfg.removeMateId = true;
    readerCfg.maxChunkSize = 100000;
    // TODO: add protocol option, default Illumina fr
    readerCfg.reverseAlignmentStrand = upstreamMates;

    ChunkBamReader bamReader(pathToAlignments.c_str(), readerCfg);

    if (!bamReader.init()) {
        cerr << "Failed to open file " << pathToAlignments << endl;
        return false;
    }

    const StringSet<CharString>& refNames = bamReader.getRefNames();

    while (!bamReader.isEof()) {
        vector<BamAlignmentRecord> records;
        if (!bamReader.loadNextChunk(records)) {
            cerr << "Failed to read data from" << pathToAlignments << endl;
            return false;
        }

        foreach_ (const BamAlignmentRecord& rec, records) {
            string readName = toCString(rec.qName);
            if (!transcriptomic && !ChunkBamReader::readCompletelytAligned(rec) ) {
                continue;
            }

            BreakpointCandidateMap::const_iterator iter = queryBreakpoints.find(readName);
            if (iter != queryBreakpoints.end()) {
                const vector<BreakpointCandidatePtr>& bps = iter->second;
                foreach_ (const BreakpointCandidatePtr& bp, bps) {
                    int idx = upstreamMates ? bp->getDownstreamAlnIndex() : bp->getUpstreamAlnIndex();
                    const QueryAlignment& aln = bp->alns[idx];
                    if (transcriptomic) {
                        string transcriptName = toCString(refNames[rec.rID]);
                        TranscriptPtr t = geneModel.getTranscript(transcriptName);
                        if (t) {
                            if (mateConditionChecker->genomicReadHasMateInTranscript(t, aln, upstreamMates, rec)) {
#ifdef DEBUG
                                cerr << "Pair for " << bp->readName << " is " << readName << endl;
#endif
                                bp->flags |= BreakpointFlags_HasProperPair;
                            }
                        }

                    } else {

                        string refName = toCString(refNames[rec.rID]);
                        // TODO: take into account direction?
                        if (refName == aln.refId && (abs(rec.beginPos - aln.refStart) < cfg.maxMateDistance )) {
                            bp->flags |= BreakpointFlags_HasProperPair;
                        }
                    }

                }

            }

        }

    }


    return true;
}



int FindSupportingMatesTask::performAnalysis()
{

    vector<Fusion> fusions;

    cerr << "Loading fusions..." << endl;

    if ( !Fusion::readFusionClusters(fusions, cfg.pathToFusions) ) {
        cerr << "Failed to read fusions from " << cfg.pathToFusions << endl;
        return -1;
    }


    cerr << "Collecting local alignments based on the origin" << endl;

    BreakpointCandidateMap upstreamLocalAlignments, downstreamLocalAlignments;

    foreach_ (const Fusion& f, fusions) {
        auto bps = f.getBreakpoints();

        // the supporting pair could be inside of fusion
        map<string,int> commonPairs;

        foreach_ (const BreakpointCandidatePtr& bp, bps) {
            string readName = StringUtils::removeReadMateIdentifer(bp->readName);
            commonPairs[readName] += 1;

        }

        foreach_ (const BreakpointCandidatePtr& bp, bps ) {
            string readName = StringUtils::removeReadMateIdentifer(bp->readName);

            int numSupporters = commonPairs[readName];
            if (numSupporters == 2) {
                // pair already in fusion
                bp->flags |= BreakpointFlags_HasProperPair;
                continue;
            }

            if ( bp->isOriginLocalFirstMate() ) {
                upstreamLocalAlignments[readName].push_back(bp);
            } else if ( bp->isOriginLocalSecondMate() ) {
                downstreamLocalAlignments[readName].push_back(bp);
            }
        }





    }


    if (cfg.pathToGenomicAlns2.size() > 0) {
        bool ok = findMatesFromAlignments(upstreamLocalAlignments, true, cfg.pathToGenomicAlns2, false);
        if (!ok) {
            return -1;
        }
    }

    if (cfg.pathToGenomicAlns1.size() > 0) {
        bool ok = findMatesFromAlignments(downstreamLocalAlignments, false, cfg.pathToGenomicAlns1, false);
        if (!ok) {
            return -1;
        }
    }


    if (cfg.pathToGtf.size() > 0) {

        cerr << "Loading transcript annotations..." << endl;

        if (! geneModel.loadTranscripts(cfg.pathToGtf)) {
            cerr << "Failed to load gene models from " << cfg.pathToGtf << endl;
            return -1;
        }


        cerr << "Searching for upstream mates in transcriptomic alignments" << endl;

        if (!findMatesFromAlignments(upstreamLocalAlignments, true, cfg.pathToTranscriptomeAlns2, true) ){
            cerr << "Failed to search for upstream mates" << endl;
            return -1;
        }


        cerr << "Searching for downstream mates in transcriptomic alignments"  << endl;

        if (!findMatesFromAlignments(downstreamLocalAlignments, false, cfg.pathToTranscriptomeAlns1, true) ){
            return -1;
        }
    }

    upstreamLocalAlignments.clear();
    downstreamLocalAlignments.clear();

    cerr << "Writing results" << endl;

    if (!Fusion::writeFusionClusters(fusions, cfg.pathToOutputFusions ) ) {
        cerr << "Failed to write results to " << cfg.pathToOutputFusions << endl;
        return -1;
    }

    return 0;
}


bool GeneBoundaryMateChecker::genomicReadHasMateInTranscript(const TranscriptPtr &transcript, const QueryAlignment &aln, bool upstreamMates, const BamAlignmentRecord &possibleMate)
{
    AlignmentStrand possibleMateStrand = hasFlagRC(possibleMate) ? AlignmentStrand_Reverse : AlignmentStrand_Forward;

    AlignmentStrand alnStrandInTranscript = aln.positiveStrand == transcript->isPositiveStranded() ? AlignmentStrand_Forward : AlignmentStrand_Reverse;

    // We imply that mates protocols are unified to FORWARD-FORWARD

    if ( alnStrandInTranscript != possibleMateStrand ) {
        return false;
    }

    if (transcript->getRefName() != aln.refId) {
        return false;
    }

    SimpleInterval testRegion;

    bool strandCondition = alnStrandInTranscript == AlignmentStrand_Forward;
    if (upstreamMates == strandCondition) {
        testRegion.begin = transcript->isPositiveStranded() ? transcript->getGene()->getStartPos() : transcript->getGene()->getEndPos();
        testRegion.end = transcript->getGenomicPos(possibleMate.beginPos);
    } else {
        int alnEndPos = getAlignmentLengthInRef(possibleMate) + possibleMate.beginPos;
        testRegion.begin = transcript->getGenomicPos(alnEndPos);
        testRegion.end = transcript->isPositiveStranded() ? transcript->getGene()->getEndPos() :transcript->getGene()->getStartPos();
    }


    if (testRegion.begin > testRegion.end) {
        std::swap(testRegion.begin, testRegion.end);
    }

    SimpleInterval genomicMateInterval(aln.refStart, aln.inRefEnd());

    return genomicMateInterval.intersects(testRegion);

}


bool InsertSizeBasedMateChecker::genomicReadHasMateInTranscript(const TranscriptPtr &transcript, const QueryAlignment &aln, bool isUpstreamMate, const BamAlignmentRecord &possibleMate)
{
    AlignmentStrand possibleMateStrand = hasFlagRC(possibleMate) ? AlignmentStrand_Reverse : AlignmentStrand_Forward;

    AlignmentStrand alnStrandInTranscript = aln.positiveStrand == transcript->isPositiveStranded() ? AlignmentStrand_Forward : AlignmentStrand_Reverse;

    // We imply that mates protocols are unified to FORWARD-FORWARD

    if ( alnStrandInTranscript != possibleMateStrand ) {
        return false;
    }

    if (transcript->getRefName() != aln.refId) {
        return false;
    }

    SimpleInterval testRegion;

    bool strandCondition = alnStrandInTranscript == AlignmentStrand_Forward;
    if (isUpstreamMate == strandCondition) {
        testRegion.begin = transcript->getGenomicPos(possibleMate.beginPos);
        testRegion.end = transcript->isPositiveStranded() ? testRegion.begin - maxMateDistance : testRegion.begin + maxMateDistance;
    } else {
        int alnEndPos = getAlignmentLengthInRef(possibleMate) + possibleMate.beginPos;
        testRegion.begin = transcript->getGenomicPos(min(alnEndPos,transcript->getLength() - 1));
        testRegion.end = transcript->isPositiveStranded() ? testRegion.begin + maxMateDistance :testRegion.begin - maxMateDistance;
    }


    if (testRegion.begin > testRegion.end) {
        std::swap(testRegion.begin, testRegion.end);
    }

    SimpleInterval genomicMateInterval(aln.refStart, aln.inRefEnd());

    return genomicMateInterval.intersects(testRegion);

}


