#ifndef GLOBAL_CONTEXT_H
#define GLOBAL_CONTEXT_H

#include <boost/scoped_ptr.hpp>

#include "Reference.h"

class GlobalContext
{
    static GlobalContext* ctx;
private:
    boost::scoped_ptr<ReferenceSeq> genomeRef;
public:
    GlobalContext() {}


    ReferenceSeq* getGenomeReference() { return genomeRef.get(); }
    void setGenomeReference(ReferenceSeq* ref) { genomeRef.reset(ref); }

    static GlobalContext* getGlobalContext() { return ctx; }
    static void setGlobalContext(GlobalContext* _ctx) { ctx = _ctx; }
};

#endif // GLOBAL_CONTEXT_H
