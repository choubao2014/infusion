#ifndef GENOMIC_OVERLAP_DETECTOR_H
#define GENOMIC_OVERLAP_DETECTOR_H

#include <map>
#include <vector>
#include "IntervalTree.h"

template<typename TValue>
class GenomicOverlapDetector {
    std::map<std::string,IntervalTree<TValue> > intervalMap;
public:
    void findIntervals(std::vector<TValue> &result, const std::string &refId, int begin, int end)
    {
        if (intervalMap.count(refId) == 0) {
            return;
        }

        const IntervalTree<TValue>& intervalTree = intervalMap[refId];
        intervalTree.findOverlapers(result, begin, end);

    }

    void insertInterval(const std::string& refId, int begin, int end, const TValue& val)
    {
        intervalMap[refId].insertInterval(begin, end, val);
    }

    const std::map<std::string,IntervalTree<TValue> >& getIntervalMap() const { return intervalMap; }

    void clear() { intervalMap.clear(); }

    const IntervalTree<TValue>& getOverlapper(const string& refId) const { return intervalMap.at(refId); }

    /*void debugPrint()
    {

        for ( auto iter = intervalMap.begin(); iter != intervalMap.end(); ++iter ) {
            cout << "Ref: " << iter->first;
            IntervalTree<FusionSegmentPtr>& iTree = iter->second;
            auto node = iTree.leftMostNode();
            while (node) {
                cout << node->getIndex() << ": begin=" << node->getStart() << ", end=" <<  node->getEnd() << ", clusterIdx=" << node->getValue()->getIdx() << endl;
                node = iTree.stepForward(node);
            }


        }
    }*/

};



#endif // GENOMIC_OVERLAP_DETECTOR_H
