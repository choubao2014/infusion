#include "StringUtils.h"
#include "FeatureStreamReader.h"

#include <iostream>

using namespace std;


FeatureStreamReader::FeatureStreamReader()
{
}

bool FeatureStreamReader::open(const string &filePath)
{
    infile.open(filePath);
    return infile.is_open();
}

bool FeatureStreamReader::readAll(FeatureSet &featureSet, const string &filePath)
{

    while (infile.good()) {
        string line;
        getline(infile,line);
        if (StringUtils::trim(line).length() == 0 || line[0] == '#' ) {
            continue;
        }

        FeaturePtr feature = parseLine(line);

        if ( ! feature) {
            cerr << getError() << endl;
            continue;
        }

        featureSet[feature->id] =  feature;

    }


    return featureSet.size() > 0;

}


FeaturePtr GffStreamReader::parseLine(const string &line)
{
    vector<string> items = StringUtils::split(line,'\t');

    if (items.size() != 9) {
        string msg;
        msg.append( "GFF format error: there should at least 9 tab-separated fields in each record\n");
        msg.append("Problematic line is ").append(line);
        setError(msg);
        return FeaturePtr();
    }


    string seqName = items[0];
    string featureName = items[2];
    bool ok = true;
    int start = StringUtils::parseNumber<int>(items[3], ok); // 1-based
    if (!ok) {
        setError("Failed to parse coordinated");
        return FeaturePtr();
    }

    int end = StringUtils::parseNumber<int>(items[4], ok); // 1-based, inclusive
    if (!ok) {
        setError("Failed to parse end of interval");
        return FeaturePtr();
    }

    bool isPositiveStrand =  items[6] == "+";


    FeaturePtr feature( new FeatureData(featureName, featureName, start, end, isPositiveStrand) );

    /*string[] attrs = items[8].trim().split(";");

    for (string attr: attrs) {
                           String[] pair = attr.trim().split(" ");
                           if (pair.length < 2) {
                               setError("GTF format error: attributes are missing.\n" +
                                       "Problematic line is " + line);
                           }
                           String value = pair[1];
                           //remove surrounding quotes
                           if (value.startsWith("\"") && value.endsWith("\"")) {
                               value = value.substring(1, value.length() - 1);
                           }

                           feature.addAttribute(pair[0], value);
                       }

                       return feature;
    */


    return feature;

}

bool FeatureStreamReader::load(FeatureSet &featureSet, const string &filePath)
{
    GffStreamReader reader;

    if (!reader.open(filePath)) {
        return false;
    }

    return reader.readAll(featureSet, filePath);


}





