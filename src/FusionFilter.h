#ifndef FUSION_FILTER_H
#define FUSION_FILTER_H

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>

#include <seqan/sequence.h>

#include "AlignmentUtils.h"

using std::string;
using std::map;
using seqan::CharString;
using seqan::StringSet;

class Fusion;

enum FusionJunctionType {
    FUSION_JUNCTION_KNOWN = 0,
    FUSION_JUNCTION_INTRONIC = 1,
    FUSION_JUNCTION_INTERGENIC = 2
};

class FusionFilter
{
    string name;
public:
    struct Result {
        bool passed;
        int score;
        string message;
        Result() : passed(true), score(0) {}
     };

    FusionFilter(const string& filterName) : name(filterName) {}
    const string& getName() const { return name; }
    virtual ~FusionFilter() {}
    virtual Result process(Fusion& f) = 0;
};


struct MinSupportFilterConfig {
    int minSplitReads;
    int minSpanPairs;
    int minFragments;
    int minNotRescuedReads;
    int minSplitReadsNovelJunction;


    MinSupportFilterConfig() {
        minSplitReads = 1;
        minSpanPairs = 0;
        minFragments = 4;
        minNotRescuedReads = 0;
        minSplitReadsNovelJunction = 3;

    }
};


class MinSupportFilter : public FusionFilter
{
    const map<int,FusionJunctionType>& fusionJunctionMap;
    MinSupportFilterConfig cfg;
    bool isPaired;
public:
    MinSupportFilter(const map<int,FusionJunctionType>& junctionMap, const MinSupportFilterConfig& config, bool pairedData  )
        : FusionFilter("min_support"), fusionJunctionMap(junctionMap), cfg(config), isPaired(pairedData) { }
    virtual Result process(Fusion &f);
};

class ReferenceSeq;

class HomologyFilter : public FusionFilter {
public:
    HomologyFilter();
    ~HomologyFilter();
    static AlignmentResult parseAnnotation(const string& annotion);
    virtual Result process(Fusion &f);
};

class InsertSizeFilter : public FusionFilter {
    float minPAPRate;
public:
    InsertSizeFilter(float papRate) : FusionFilter("properly_aligned_pairs_rate"), minPAPRate(papRate) {}
    virtual Result process(Fusion& f);

};

class CoverageContextFilter : public FusionFilter {
    //float minBckgCoverage;
    int minBckgReads;
public:
    CoverageContextFilter(float minBackgroundReads) : FusionFilter("coverage_context"), minBckgReads(minBackgroundReads){}
    virtual Result process(Fusion&f);
};

class SplitCoverageFilter : public FusionFilter {
    float minUniqReadRate;
    int minUniqueSplitReads;
public:
    SplitCoverageFilter(float uniqReadRate, int minUniqueSplits) : FusionFilter("unqiue_start_rate"), minUniqReadRate(uniqReadRate), minUniqueSplitReads(minUniqueSplits) {}
    virtual Result process(Fusion& f);

};

class SplitPositionFilter : public FusionFilter {
    float lowerBound, upperBound;
public:
    SplitPositionFilter(float splitPosDev) : FusionFilter("mean_split_pos"), lowerBound(0.5 - splitPosDev), upperBound(0.5 + splitPosDev)  {}
    virtual Result process(Fusion& f);
};

class HomogeneityFilter : public FusionFilter {
    float minHomogeneityWeight;
    float requiredHomogeneityWeight;
public:
    HomogeneityFilter(float minWeight, float reqWeight) : FusionFilter("homogeneity"), minHomogeneityWeight(minWeight), requiredHomogeneityWeight(reqWeight)  {}
    virtual Result process(Fusion& f);
};


struct NonCodingRegionFilterConfig {
    std::set<string> excludedBiotypes;
    bool allowNonCoding;
    bool allowIntergenic;
    bool allowIntronic;
    NonCodingRegionFilterConfig() {
        allowIntergenic = false;
        allowIntronic = false;
        allowNonCoding = false;
    }
};

class NonCodingRegionFilter : public FusionFilter
{
    NonCodingRegionFilterConfig cfg;
    map<int,FusionJunctionType>& fusionJunctionMap;
public:
    NonCodingRegionFilter(const NonCodingRegionFilterConfig& config, map<int,FusionJunctionType>& junctionMap) :
        FusionFilter("non_coding_region"), cfg(config), fusionJunctionMap(junctionMap) {}

    virtual Result process(Fusion &f);
};


class ParalogsFilter : public FusionFilter
{
    std::set<string> paralogs;
    bool loadedParalogs;
    bool loadParalogs(const string& inPath);
public:
    ParalogsFilter(const string& pathToParologsList);
    virtual Result process(Fusion &f);

};

typedef boost::shared_ptr<FusionFilter> FusionFilterPtr;


#endif // FUSION_FILTER_H
